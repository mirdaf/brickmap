package BrickMapPackage;

import java.util.*;

/**
 * Содержит географические координаты.
 */
public class Point {

    /**
     * Содержит географические координаты.
     */
    public Point() {
    }

    /**
     * 
     */
    public Double Latitude;

    /**
     * 
     */
    public Double Longitude;


}