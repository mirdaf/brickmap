-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE BricksDb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE GetBrickByMark
	@mark_id int,
	@id int OUT,
	@stamp nvarchar(53) OUT,
	@has_number bit OUT,
	@number int  = NULL OUT,
	@quality int OUT,
	@privacy int OUT,
	@discovery_date datetime OUT
AS
BEGIN
	SELECT @id = Id, @stamp = Stamp, @has_number = HasNumber, @number = Number,
		@quality = Quality, @privacy = PrivacyStatus, @discovery_date = DiscoveryDate
	FROM dbo.Bricks
	WHERE Mark_Id = @mark_id;
END
GO
