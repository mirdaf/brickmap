USE u0169325_bricksdb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE SaveNewPassword
	@user_id int,
	@hash nvarchar(max)
AS
BEGIN
	UPDATE dbo.Users
	SET Hash = @hash
	WHERE Id = @user_id;
END
GO
