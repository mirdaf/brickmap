--USE [u0169325_bricksdb]
USE [BricksDb]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 06.08.2016
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddDraftPhoto]
	@img_path nvarchar(128),
	@upload_date datetime
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO dbo.DraftPhotos(Path, UploadDate)
	VALUES (@img_path, @upload_date);

END
GO
