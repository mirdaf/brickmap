USE [BricksDb]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 2016-11-20
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE DeletePhoto
	@brick_id	int,
	@photo		nvarchar(256)
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM dbo.BricksPhotos
	WHERE Brick_Id = @brick_id
	AND Photo_Id IN 
		(SELECT Id FROM dbo.Photos
		WHERE Path = @photo);
	
	IF (0 = 
		(SELECT COUNT(br_ph.Brick_Id)
		FROM dbo.BricksPhotos AS br_ph
		INNER JOIN dbo.Photos AS ph
			ON br_ph.Photo_Id = ph.Id
		WHERE ph.Path = @photo))
	BEGIN 
		DELETE FROM dbo.Photos
		WHERE Path = @photo;
	END;
	
	
END
GO
