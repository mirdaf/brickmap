--USE [u0169325_bricksdb]
USE [BricksDb]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 2016-08-06
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[SaveEditedBrick]
	@stamp nvarchar(53),
	@numero int = NULL,
	@has_number bit,
	@quality int,
	@disc_date datetime,
	@privacy int,
	@user_id int, 
	@new_img_path nvarchar(128) = '',
	@brick_id int
AS
	DECLARE @photo_id int
	DECLARE @old_img_path nvarchar(128)
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.Bricks
	SET Stamp = @stamp,
		HasNumber = @has_number,
		Quality = @quality,
		DiscoveryDate = @disc_date,
		Number = @numero,
		PrivacyStatus = @privacy
	WHERE Id = @brick_id;

	SELECT @old_img_path = ph.Path
	FROM dbo.Photos AS ph
	INNER JOIN dbo.BricksPhotos AS br_ph
	ON ph.Id = br_ph.Photo_Id
	WHERE br_ph.Brick_Id = @brick_id;

	IF (@new_img_path <> '' AND 
		(@old_img_path IS NULL OR @new_img_path <> @old_img_path))
		BEGIN

			IF (@old_img_path IS NOT NULL)
				BEGIN 
					DELETE FROM dbo.Photos
					WHERE Path = @old_img_path;
					
					DELETE FROM dbo.BricksPhotos
					WHERE brick_id = @brick_id;
				END;

			INSERT INTO dbo.Photos(User_id, Path, UploadDate)
			VALUES (@user_id, @new_img_path, getDate());

			SET @photo_id = SCOPE_IDENTITY();

			INSERT INTO dbo.BricksPhotos(Photo_Id, Brick_Id)
			VALUES (@photo_id, @brick_id);

			DELETE FROM dbo.DraftPhotos
			WHERE Path = @new_img_path;

		END;

	IF (@old_img_path IS NOT NULL)
		BEGIN 
			INSERT INTO dbo.DraftPhotos(Path, UploadDate)
			VALUES (@old_img_path, GetDate());
		END
END
GO
