-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
--USE BricksDb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE AddGuidForRecovery
	@guid nvarchar(max),
	@user_id int
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO dbo.PasswordResetRequests(Id, User_Id, RequestDate)
	VALUES (@guid, @user_id, GetDate());
	
END
GO
