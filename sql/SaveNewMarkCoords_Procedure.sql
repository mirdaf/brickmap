USE BricksDb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE SaveNewMarkCoords
	@mark_id int,
	@lat float, 
	@lon float
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE dbo.MapMarks
	SET Coordinates_Latitude = @lat, 
	Coordinates_Longitude = @lon
	WHERE Id = @mark_id
	
END
GO
