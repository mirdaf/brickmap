--USE [u0169325_bricksdb]
USE [BricksDb]
GO

/****** Object:  Table [dbo].[DraftPhotos]    Script Date: 06.08.2016 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].DraftPhotos(
	[Path] [nvarchar](max) NOT NULL,
	[UploadDate] [datetime] NOT NULL
)

GO
