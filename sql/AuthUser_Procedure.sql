USE BricksDb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE AuthUser
	@email nvarchar(max),
	@hash nvarchar(max)
AS
BEGIN
    SELECT Id, Login
	FROM dbo.Users
	WHERE EMail = @email
	AND Hash = @hash;
	
END
GO
