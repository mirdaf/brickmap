
/****** Object:  Table [dbo].[PasswordResetRequests]    Script Date: 23.05.2016 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PasswordResetRequests](
	[Id] [nvarchar](max) NOT NULL,
	[User_Id] [int] NULL,
	[RequestDate] [datetime] NOT NULL)
GO

ALTER TABLE [dbo].[PasswordResetRequests]  WITH CHECK ADD CONSTRAINT [FK_dbo.Bricks_dbo.PasswordReq_UserId] FOREIGN KEY([User_Id])
REFERENCES [dbo].[Users] ([Id])
GO

ALTER TABLE [dbo].[PasswordResetRequests] CHECK CONSTRAINT [FK_dbo.Bricks_dbo.PasswordReq_UserId]
GO


