USE [u0169325_bricksdb]
--USE [BricksDb]
GO
/****** Object:  StoredProcedure [dbo].[GetBrickByMark]    Script Date: 05.04.2016 22:56:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetBrickByMark]
	@mark_id int,
	@id int OUT,
	@stamp nvarchar(53) OUT,
	@has_number bit OUT,
	@number int  = NULL OUT,
	@quality int OUT,
	@privacy int OUT,
	@comment nvarchar(512) OUT,
	@discovery_date datetime OUT,
	@user_id int OUT,
	@login nvarchar(53) OUT,
	@photo nvarchar(256) OUT
AS
BEGIN
	SELECT @id = b.Id, @stamp = b.Stamp, @has_number = b.HasNumber, @number = b.Number,
		@quality = b.Quality, @privacy = b.PrivacyStatus, @comment = b.Comment,
		@discovery_date = b.DiscoveryDate, @user_id = u.Id, @login = u.Login
	FROM dbo.Bricks AS b
	INNER JOIN dbo.MapMarks AS m 
		ON b.Mark_Id = m.Id
	INNER JOIN dbo.Users AS u 
		ON m.User_Id = u.Id
	WHERE b.Mark_Id = @mark_id;

	SELECT @photo = ph.Path
	FROM dbo.Photos AS ph
	INNER JOIN dbo.BricksPhotos AS br_ph
		ON ph.Id = br_ph.Photo_Id
	WHERE br_ph.Brick_Id = @id;

END
