-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [u0169325_bricksdb]
--USE [BricksDb]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[AddNewBrick]
	@lat float, 
	@lon float,
	@stamp nvarchar(53),
	@numero int = NULL,
	@has_number bit,
	@quality int,
	@disc_date datetime,
	@privacy int,
	@comment nvarchar (512),
	@user_id int, 
	@img_path nvarchar(128),
	@mark_id int OUT
AS
	DECLARE @brick_id int
	DECLARE @photo_id int
BEGIN
	SET NOCOUNT ON;

    INSERT INTO dbo.MapMarks(Coordinates_Latitude, Coordinates_Longitude, User_Id)
	VALUES (@lat, @lon, @user_id);
	SET @mark_id = SCOPE_IDENTITY();

	INSERT INTO dbo.Bricks
		(Stamp, Mark_Id, HasNumber, Quality, DiscoveryDate, Number, PrivacyStatus, RegisterDate, Comment)
	VALUES (@stamp, @mark_id, @has_number, @quality, @disc_date, @numero, @privacy, getDate(), @comment);

	SET @brick_id = SCOPE_IDENTITY();

	IF (@img_path <> '') 
		BEGIN
			INSERT INTO dbo.Photos(User_id, Path, UploadDate)
			VALUES (@user_id, @img_path, getDate());

			SET @photo_id = SCOPE_IDENTITY();

			INSERT INTO dbo.BricksPhotos(Photo_Id, Brick_Id)
			VALUES (@photo_id, @brick_id);

			DELETE FROM dbo.DraftPhotos
			WHERE Path = @img_path;

		END;
END
GO
