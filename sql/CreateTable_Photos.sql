USE [BricksDb]
GO

/****** Object:  Table [dbo].[Photos]    Script Date: 06.04.2016 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Photos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[User_Id] [int] NULL,
	[Path] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.Photos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Photos]  WITH CHECK ADD CONSTRAINT [FK_dbo.Bricks_dbo.Photos_User_Id] FOREIGN KEY([User_Id])
REFERENCES [dbo].[Users] ([Id])
GO

ALTER TABLE [dbo].[Photos] CHECK CONSTRAINT [FK_dbo.Bricks_dbo.Photos_User_Id]
GO


