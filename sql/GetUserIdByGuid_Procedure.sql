--USE BricksDb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE GetUserIdByGuid
	@guid nvarchar(max)
AS
BEGIN
    SELECT User_Id, RequestDate
	FROM dbo.PasswordResetRequests
	WHERE Id = @guid;
END
GO
