-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE BricksDb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SaveEditedUser
	@email nvarchar(max),
	@hash nvarchar(max),
	@login nvarchar(max)
AS
BEGIN
	UPDATE dbo.Users
	SET Login = @login, Hash = @hash
	WHERE EMail = @email;
END
GO
