-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE BricksDb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE AddUser
	@email nvarchar(max),
	@hash nvarchar(max),
	@login nvarchar(max),
	@user_id int OUT
AS
BEGIN
	SET NOCOUNT ON;

    INSERT INTO dbo.Users(Login, Hash, EMail, RegisterDate)
	VALUES (@login, @hash, @email, getDate());
	SET @user_id = SCOPE_IDENTITY();
	
END
GO
