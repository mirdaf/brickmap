-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [BricksDb]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE DeleteMark
	@mark_id int
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM dbo.Photos
	WHERE Id IN (
		SELECT br_ph.Photo_Id
		FROM dbo.Bricks br
		INNER JOIN dbo.BricksPhotos br_ph
			ON br.Id = br_ph.Brick_Id
		WHERE br.Mark_Id = @mark_id
		);

	DELETE FROM dbo.Bricks
	WHERE Mark_Id = @mark_id;

	DELETE FROM dbo.MapMarks
	WHERE Id = @mark_id;
END
GO
