--USE [u0169325_bricksdb]
USE [BricksDb]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 06.08.2016
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetNotUsedPhotos]
AS
BEGIN

	SELECT Path
	FROM dbo.DraftPhotos
	WHERE UploadDate < DATEADD(day, -1, GETDATE());

END
GO
