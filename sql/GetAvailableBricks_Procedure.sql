-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE BricksDb
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE GetAvailableBricks
	@user_id int
AS
BEGIN
	SELECT marks.Id, marks.Coordinates_Latitude, marks.Coordinates_Longitude
	FROM dbo.MapMarks marks
	INNER JOIN dbo.Bricks bricks
	ON marks.Id = bricks.Mark_Id
	WHERE marks.User_Id = @user_id
		OR bricks.PrivacyStatus = 3
		OR (@user_id != -1 AND bricks.PrivacyStatus = 2)
END
GO
