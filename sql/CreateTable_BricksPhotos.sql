--USE [u0169325_bricksdb]
USE [BricksDb]
GO

/****** Object:  Table [dbo].[BricksPhotos]    Script Date: 06.04.2016 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--CREATE TABLE [dbo].[BricksPhotos](
--	[Photo_Id] [int] NOT NULL,
--	[Brick_Id] [int] NOT NULL
--)

--GO

--CONSTRAINT [FK_Table_3_Table_1] FOREIGN KEY([t1]) REFERENCES [dbo].[Table_1] ([t1]),
--    CONSTRAINT [FK_Table_3_Table_2] FOREIGN KEY([t2]) REFERENCES [dbo].[Table_2] ([t2])


ALTER TABLE [dbo].[BricksPhotos] DROP CONSTRAINT [FK_dbo.Bricks_dbo.BricksPhotos_Photo_Id] 
GO

ALTER TABLE [dbo].[BricksPhotos] WITH CHECK 
ADD CONSTRAINT [FK_dbo.Bricks_dbo.BricksPhotos_Photo_Id] 
FOREIGN KEY([Photo_Id])
REFERENCES [dbo].[Photos] ([Id])
ON DELETE CASCADE 
GO

--ALTER TABLE [dbo].[BricksPhotos]  WITH CHECK ADD CONSTRAINT [FK_dbo.Bricks_dbo.BricksPhotos_Brick_Id] FOREIGN KEY([Brick_Id])
--REFERENCES [dbo].[Bricks] ([Id])
--GO


