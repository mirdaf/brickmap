--USE [u0169325_bricksdb]
USE [BricksDb]
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: 2016-08-06
-- Description:	<Description,,>
-- =============================================
CREATE TRIGGER [dbo].[OnDeletePhoto]
ON [dbo].[Photos]
FOR DELETE 
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT INTO dbo.DraftPhotos(Path, UploadDate)
		SELECT Path, GetDate()
		FROM deleted

END
GO
