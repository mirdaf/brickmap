﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrickMaster.Master" AutoEventWireup="true" 
    CodeBehind="Register.aspx.cs" Inherits="BrickMap.Register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>BrickMap - Регистрация</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="registerForm" class="regform">
        <div class="reg-row"><div class="reg-label">E-mail:</div>
            <asp:TextBox ID="txtEmail" runat="server" TextMode="Email" CssClass="regtext" />
            <asp:RequiredFieldValidator ID="LoginRequiredValidator"
                                    runat="server" ErrorMessage="*"
                                    ControlToValidate="txtEmail" ForeColor="Red" /></div>
        <div class="reg-row"><div class="reg-label">Login:</div>
            <asp:TextBox ID="txtLogin" runat="server" CssClass="regtext" /></div>
        <div class="reg-row"><div class="reg-label">Password:</div>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="regtext" />
            <asp:RequiredFieldValidator ID="PwdRequiredValidator"
                                    runat="server" ErrorMessage="*"
                                    ControlToValidate="txtPassword" ForeColor="Red" /></div>
        <asp:Button ID="submitRegister" runat="server" CssClass="panel-btn light-text" Text="Зарегистрироваться" /><br />
        <asp:Label ID="txtMessage" runat="server" />
    </div>
</asp:Content>
