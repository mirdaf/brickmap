﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrickMap
{
    public partial class ResetPassword : System.Web.UI.Page
    {
        LoginService loginService = new LoginService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (BrickCookiesHelper.IsUserLoggedIn())
                Response.Redirect("~/Default.aspx");
            if (!Page.IsPostBack)
            {
                string guid = Request.QueryString["guid"];
                if (String.IsNullOrEmpty(guid))
                    Response.Redirect("~/Default.aspx");
                int user_id = loginService.GetUserIdByGuid(guid);
                if (user_id == -1)
                    Response.Redirect("~/Default.aspx");
                else
                {
                    if (Session["user_id"] == null)
                    {
                        Session["user_id"] = user_id;
                    }               
                }
            }
            submitSetNewPassword.Click += (o, ev) => TrySavePassword();
        }
        private void TrySavePassword()
        {
            txtErrorMessage.Text = "";
            bool result = false;
            int user_id = (int)Session["user_id"];
            if (String.IsNullOrEmpty(txtPassword.Text) && String.IsNullOrEmpty(txtPasswordDouble.Text))
            {
                txtErrorMessage.Text = "Введите пароль в обоих полях";
            }
            else
            {
                if (String.Compare(txtPassword.Text, txtPasswordDouble.Text) != 0)
                {
                    ComparePasswordsValidator.IsValid = false;
                    return;
                }
                string hash = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword.Text, "SHA1");
                result = loginService.SaveNewPassword(user_id, hash);
            }
            if (result)
                ShowErrorMessage("Пароль успешно сохранен.");
            else
                ShowErrorMessage("Не удалось сохранить изменения, повторите попытку позже.");
        }
        private void ShowErrorMessage(string msg)
        {
            txtMessage.Text = msg;
        }
    }
}