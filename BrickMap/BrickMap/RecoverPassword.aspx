﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BrickMaster.Master" AutoEventWireup="true" CodeBehind="RecoverPassword.aspx.cs" Inherits="BrickMap.RecoverPassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

        <div id="recoverPasswordForm" class="regform">
            <div class="reg-row">
                <div class="reg-label">Ваш e-mail:</div>
                <asp:TextBox ID="txtEmail" runat="server" TextMode="Email" CssClass="regtext" />
            </div>

            <asp:Label ID="txtErrorMessage" runat="server" CssClass="error-main" />
            <asp:Button ID="confirmRecoverPassword" runat="server" CssClass="panel-btn light-text" Text="Восстановить пароль" /><br />
            <asp:Label ID="txtMessage" runat="server" />
    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="bottomContent" runat="server">
</asp:Content>
