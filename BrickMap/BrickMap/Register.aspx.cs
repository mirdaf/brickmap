﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrickMap
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (BrickCookiesHelper.IsUserLoggedIn())
                Response.Redirect("~/Profile.aspx");

            submitRegister.Click += (o, ev) => TryRegister();
        }

        private void TryRegister()
        {
            txtMessage.Text = "";
            string login = txtLogin.Text;
            string email = txtEmail.Text;
            string password = txtPassword.Text;
            string hashed = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");

            int regResult = RegisterInDB(email, hashed, login);
            if (regResult > 0)
                RegSucceeded(regResult, email, login);
            else
                RegFailed(regResult);
        }

        private int RegisterInDB(string email, string hashed, string login)
        {
            if (String.IsNullOrEmpty(login))
                login = email;
            return (new LoginService()).CreateUser(email, hashed, login);
        }

        private void RegSucceeded(int user_id, string email, string login)
        {
            DB.User user = new DB.User()
            {
                Id = user_id,
                Email = email,
                Login = login
            };
            BrickCookiesHelper.RegisterBrickUserCookies(user, false);
            Response.Redirect(FormsAuthentication.DefaultUrl);
        }

        private void RegFailed(int regResult)
        {
            switch (regResult)
            {
                case -2:
                    txtMessage.Text = "Кто-то с таким email уже зарегистрировался раньше вас";
                    break;
                case -1:
                    txtMessage.Text = "Какая-то проблема с сервером базы данных, попробуйте позже";
                    break;
                default:
                    break;
            }
        }
    }
}