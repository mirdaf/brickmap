﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.Services;

namespace BrickMap
{
    /// <summary>
    /// Summary description for LoginService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LoginService : System.Web.Services.WebService
    {
        // If trying to insert duplicated value of email,
        // sql server errors appears:
        // 2627 - Violation of %ls constraint '%.*ls'. Cannot insert duplicate key in object '%.*ls',
        // 3621 - The statement has been terminated.
        // Returns: user ID, if registration succeeded, 
        // -2, if user with the email already exists in DB, 
        // -1, if some else database error occurs.
        [WebMethod]
        public int CreateUser(string email, string hash, string login)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            int user_id = -1;
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("AddUser", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @email
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = email;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @hash
                param.ParameterName = "@hash";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = hash;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @login
                param.ParameterName = "@login";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = login;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                SqlParameter param_user_id = new SqlParameter(); // @user_id
                param_user_id.ParameterName = "@user_id";
                param_user_id.SqlDbType = SqlDbType.Int;
                param_user_id.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_user_id);

                // Выполнение хранимой процедуры.
                cmd.ExecuteNonQuery();
                user_id = (int)param_user_id.Value;
            }
            catch (SqlException e)
            {
                foreach (SqlError error in e.Errors)
                {
                    if (error.Number == 2627)
                    {
                        user_id = -2;
                        break;
                    }
                    user_id = -1;
                }
            }
            BrickMailService.UserRegistered(user_id, email, login);
            return user_id;
        }

        // Returns:
        // user ID, if login and password are correct,
        // -1, if there is no user with specified email and hash.
        [WebMethod]
        public BrickMap.DB.User AuthUser(string email, string hash)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            int user_id = -1;
            string login = "";
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("AuthUser", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @email
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = email;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @hash
                param.ParameterName = "@hash";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = hash;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                SqlDataReader reader = cmd.ExecuteReader();

                if(reader.Read())
                {
                    int.TryParse(reader["Id"].ToString(), out user_id);
                    login = reader["Login"].ToString();
                }
                reader.Close();
            }
            catch (SqlException e)
            {
                string err = "";
                foreach (SqlError error in e.Errors)
                {
                    err += error.Number.ToString() + " ";
                }
                throw new ApplicationException(err);
            }
            return new BrickMap.DB.User()
                {
                    Id = user_id,
                    Email = email,
                    Login = login
                };
        }

        [WebMethod]
        public bool SaveEditedUser(string email, string hash, string login)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SaveEditedUser", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @email
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = email;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @hash
                param.ParameterName = "@hash";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = hash;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @login
                param.ParameterName = "@login";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = login;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                // Выполнение хранимой процедуры.
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }

        [WebMethod]
        public bool SaveNewLogin(string email, string login)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SaveNewLogin", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @email
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = email;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @login
                param.ParameterName = "@login";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = login;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                // Выполнение хранимой процедуры.
                var res = cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }
        // Checks if user with specified email is registered.
        // If true, returns user ID. 
        // Otherwise returns -1.
        public int CheckIfEmailIsRegistered(string email)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            int user_id = -1;
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("CheckExistingEmail", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @email
                param.ParameterName = "@email";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = email;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    int.TryParse(reader["Id"].ToString(), out user_id);
                }
                reader.Close();
            }
            catch (SqlException e)
            {
                string err = "";
                foreach (SqlError error in e.Errors)
                {
                    err += error.Number.ToString() + " ";
                }
                throw new ApplicationException(err);
            }
            return user_id;
        }
        public string CreateGuidForRecovery(int user_id)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            string guid = "" + Guid.NewGuid();
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("AddGuidForRecovery", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @guid
                param.ParameterName = "@guid";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = guid;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @user_id
                param.ParameterName = "@user_id";
                param.SqlDbType = SqlDbType.Int;
                param.Value = user_id;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                var res = cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                string err = "";
                foreach (SqlError error in e.Errors)
                {
                    err += error.Number.ToString() + " ";
                }
                throw new ApplicationException(err);
            }
            return guid;
        }
        // Returns:
        // user ID, if user with guid resquested password recovery,
        // otherwise returns -1.
        [WebMethod]
        public int GetUserIdByGuid(string guid)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            int user_id = -1;
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetUserIdByGuid", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @guid
                param.ParameterName = "@guid";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = guid;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    int.TryParse(reader["User_Id"].ToString(), out user_id);
                    //login = reader["RequestDate"].ToString();
                }
                reader.Close();
            }
            catch (SqlException e)
            {
                string err = "";
                foreach (SqlError error in e.Errors)
                {
                    err += error.Number.ToString() + " ";
                }
                throw new ApplicationException(err);
            }
            return user_id;
        }
        // Returns true, if saving new password succeeded.
        // Otherwise returns false.
        [WebMethod]
        public bool SaveNewPassword(int user_id, string hash)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SaveNewPassword", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param_user_id = new SqlParameter(); // @user_id
                param_user_id.ParameterName = "@user_id";
                param_user_id.SqlDbType = SqlDbType.Int;
                param_user_id.Value = user_id;
                cmd.Parameters.Add(param_user_id);

                SqlParameter param = new SqlParameter(); // @hash
                param.ParameterName = "@hash";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = hash;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                return false;
            }
            return true;
        }
    }
}
