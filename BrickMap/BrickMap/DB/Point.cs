﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrickMap.DB
{
    public class Point
    {
        public Point (double x, double y)
        {
            Latitude = x;
            Longitude = y;
        }
        public Point()
        {
            Latitude = 0;
            Longitude = 0;
        }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

    }
}