﻿using System;

namespace BrickMap.DB
{
    public enum QualityStars
    {
        Null, One, Two, Three, Four, Five
    }
    public enum PrivacyStatus
    {
        Private = 0, 
        FriendsOnly = 1, 
        ForRegistered = 2,
        Public = 3
    }
    public class Brick
    {
        public int Id { get; set; }
        public string Stamp { get; set; }
        public int? Number { get; set; }
        public bool HasNumber { get; set; }
        public QualityStars Quality { get; set; }
        public DateTime DiscoveryDate { get; set; }
        //public List<Photo> Photos { get; set; }
        public string Photo { get; set; }
        //public User Author { get; set; }
        public MapMark Mark { get; set; }
        public PrivacyStatus Privacy { get; set; }
        public string Comment { get; set; }

    }
}