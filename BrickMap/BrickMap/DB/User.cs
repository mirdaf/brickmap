﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrickMap.DB
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Hash { get; set; }
        public List<MapMark> Marks { get; set; }
        public string ToJson()
        {
            return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(this);
        }
        public static User getFromJson(string json)
        {
            return new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<BrickMap.DB.User>(json);
        }
        public override string ToString()
        {
            return "User: Id: " + Id + ", Login: " + Login + ", Email: " + Email;
        }

    }
}