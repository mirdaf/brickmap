﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BrickMap.DB
{
    public class MapMark
    {
        public int Id { get; set; }
        public Point Coordinates { get; set; }
        public List<Brick> Bricks { get; set; }
        public User Author { get; set; }
    }
}