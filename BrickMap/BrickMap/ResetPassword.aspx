﻿<%@ Page Title="BrickMap - Восстановление пароля" Language="C#" MasterPageFile="~/BrickMaster.Master" AutoEventWireup="true" 
    CodeBehind="ResetPassword.aspx.cs" Inherits="BrickMap.ResetPassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="profileForm" class="regform">
        <div class="reg-row">
            <div class="reg-label">Новый пароль:</div>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="regtext" />
        </div>
        <div class="reg-row">
            <div class="reg-label">Еще раз пароль:</div>
            <asp:TextBox ID="txtPasswordDouble" runat="server" TextMode="Password" CssClass="regtext" />
            <asp:CompareValidator ID="ComparePasswordsValidator" runat="server" Type="String" Operator="Equal"
                ControlToCompare="txtPassword" ControlToValidate="txtPasswordDouble" ErrorMessage="Пароли не совпадают"
                ForeColor="Red" />
            <asp:Label ID="txtErrorMessage" runat="server" CssClass="error-main" />
        </div>

        <asp:Button ID="submitSetNewPassword" runat="server" CssClass="panel-btn light-text" Text="Сохранить новый пароль" /><br />
        <asp:Label ID="txtMessage" runat="server" />
    </div>
</asp:Content>
