﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BrickMapMenu.ascx.cs"
    Inherits="BrickMap.controls.BrickMapMenu" ClientIDMode="Static" %>

<link href='https://fonts.googleapis.com/css?family=PT+Serif:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

<div>
    <span>
        <a href="../Default.aspx" class="title menu-item">BrickMap</a>
    </span>
    <!--
    <span>
        <a href="../Links.aspx" class="menu-item">Кирпичные ссылки</a>
    </span>-->

    <span>
        <a href="#" class="menu-item menu-sandwich" id="menu-sandwich">Меню</a>
    </span>

    <span id="unsigned_box" runat="server" class="visible"><span class="top-menu-chapter">
        <a href="../Register.aspx" class="menu-right menu-item" id="register">Зарегистрироваться</a>
        <a href="../RecoverPassword.aspx" class="menu-right menu-item" id="forgot-password">Забыли пароль?</a>
        <a href="#" class="menu-right menu-item" id="login">Войти</a>
    </span></span>

    <span id="login_form" runat="server" class="visible">
        <span class="top-menu-chapter">
            <input type="hidden" runat="server" id="UserAuth" />
            <a runat="server" href="#" id="submitLogin" class="menu-right menu-item" tabindex="4" validationgroup="vgSave">Войти
            </a>
            <asp:CheckBox ID="top_rememberCheckbox" CssClass="top-checkbox menu-right menu-item" runat="server"
                TabIndex="3" ToolTip="Запомнить?" />
            <asp:RequiredFieldValidator ID="top_PwdRequiredValidator" CssClass="menu-right menu-item top-validator"
                runat="server" ErrorMessage="*" ValidationGroup="vgSave" BackColor="#0b2b3d" Display="Dynamic"
                ControlToValidate="top_Password" ForeColor="Red" />
            <asp:TextBox ID="top_Password" runat="server" TextMode="Password" CssClass="top_logintext menu-right menu-item"
                TabIndex="2" placeholder="Пароль" />
            <asp:RequiredFieldValidator ID="top_LoginRequiredValidator" CssClass="menu-right menu-item top-validator"
                runat="server" ErrorMessage="*" ValidationGroup="vgSave" BackColor="#0b2b3d" Display="Dynamic"
                ControlToValidate="top_userEmail" ForeColor="Red" />
            <asp:TextBox ID="top_userEmail" runat="server" CssClass="top_logintext menu-right menu-item"
                TabIndex="1" placeholder="Е-мэйл" />
            <a href="#" class="menu-right menu-item" id="go_back">Назад</a>
        </span>
    </span>

    <span id="logged_in_form" runat="server" class="visible">
        <span class="top-menu-chapter">
            <a runat="server" href="#" id="logout" class="menu-right menu-item" tabindex="4">Выйти
            </a>
            <a href="../Profile.aspx" id="profile" runat="server" class="menu-right menu-item"></a>
        </span>
    </span>
</div>
