﻿using BrickMap.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrickMap.controls
{
    public partial class BrickMapMenu : System.Web.UI.UserControl
    {
        private LoginService service = new LoginService();
        protected void Page_Load(object sender, EventArgs e)
        {
            submitLogin.ServerClick += (o, ev) => TryLogin();
            logout.ServerClick += (o, ev) => LogOut();

            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                unsigned_box.Attributes["class"] = "invisible";
                login_form.Attributes["class"] = "invisible";
                logged_in_form.Attributes["class"] = "visible";
                profile.InnerText = BrickCookiesHelper.GetUserData().Login;
                UserAuth.Value = "1";
            }
            else
            {
                unsigned_box.Attributes["class"] = "visible";
                login_form.Attributes["class"] = "invisible";
                logged_in_form.Attributes["class"] = "invisible";
                UserAuth.Value = "0";
            }
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            ScriptManager.RegisterClientScriptInclude(this, 
                GetType(), "menu", ResolveUrl("~/Scripts/BrickMapMenu.js"));
            ScriptManager.RegisterClientScriptInclude(this,
                GetType(), "sandwich", ResolveUrl("~/Scripts/MenuSandwich.js"));
        }

        private void TryLogin()
        {
            if (!top_LoginRequiredValidator.IsValid || !top_PwdRequiredValidator.IsValid)
            {
                return;
            }

            string email = top_userEmail.Text;
            string password = top_Password.Text;
            string hashed = FormsAuthentication.HashPasswordForStoringInConfigFile(password, "SHA1");

            User user = service.AuthUser(email, hashed);
            if (user.Id > 0)
                LoggedIn(user);
            else
                LoggingFailed();
            
            top_LoginRequiredValidator.Enabled = false;
            top_PwdRequiredValidator.Enabled = false;
        }

        private void LoggedIn(User user)
        {
            BrickCookiesHelper.RegisterBrickUserCookies(user, top_rememberCheckbox.Checked);
            UserAuth.Value = "1";
            Response.Redirect(Request.RawUrl);
        }

        private void LoggingFailed()
        {
            UserAuth.Value = "0";

            string script = "<script type='text/jscript'>ShowNotification('Неверная пара емэйл-пароль');</script>";
            Type type = GetType();
            if (!Page.ClientScript.IsStartupScriptRegistered(type, "NotLoggedInScript"))
                Page.ClientScript.RegisterStartupScript(type, "NotLoggedInScript", script);

        }
        private void LogOut()
        {
            FormsAuthentication.SignOut();
            
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName)
                {
                    Expires = DateTime.Now.AddDays(-1d)
                };
                Response.Cookies.Add(cookie);
            }

            Response.Redirect(Request.RawUrl);
        }
        
    }
}