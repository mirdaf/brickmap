﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace BrickMap
{
    public class BrickMailService
    {
        private static string smtpServer = "smtp.yandex.ru"; // "mail.brickmap.ru"; 
        private static string adminMail = "mail@brickmap.ru";
        private static string password = "zepYMlRd9oLbaYr1IQhc";
        
        private static void SendMail(string mailto, string caption, string message, string attachFile = null)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(adminMail);
                mail.To.Add(new MailAddress(mailto));
                mail.Subject = caption;
                mail.Body = message;
                mail.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(attachFile))
                    mail.Attachments.Add(new Attachment(attachFile));
                SmtpClient client = new SmtpClient();
                client.Host = smtpServer;
                //client.Port = 465; // https://yandex.ru/support/mail/mail-clients/ssl.xml
                client.Port = 25; // https://habrahabr.ru/post/237899/
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(adminMail, password);
                client.Send(mail);
                mail.Dispose();
            }
            catch (Exception e)
            {
                throw new Exception("Mail.Send: " + e.Message);
            }
        }

        public static void SendMailToRecoverPassword(string email, string link)
        {
            string caption = "Восстановление пароля для сайта BrickMap.ru";
            string message = "Вы запрашивали восстановление пароля для сайта BrickMap.ru. " +
                "Пройдите по <a href='http://" + link + "'>ссылке</a>, чтобы ввести новый пароль.";
            SendMail(email, caption, message);

        }
        public static void UserRegistered(int user_id, string email, string login)
        {
            string caption = "Новый пользователь на сайте BrickMap.ru";
            string message = "На сайте BrickMap.ru зарегистрировался пользователь с логином " + login + 
                " и электронной почтой " + email;
            SendMail(adminMail, caption, message);
        }
    }
}