﻿using BrickMap.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;

namespace BrickMap
{
    /// <summary>
    /// Summary description for MyDbService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class MyDbService : System.Web.Services.WebService
    {
        [WebMethod]
        public List<MapMark> GetBricks()
        {
            List<MapMark> marks = new List<MapMark>();

            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            int user_id = -1;
            if (BrickCookiesHelper.IsUserLoggedIn())
            {
                User user = BrickCookiesHelper.GetUserData();
                user_id = user.Id;
            }

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetAvailableBricks", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @user_id
                param.ParameterName = "@user_id";
                param.SqlDbType = SqlDbType.Int;
                param.Value = user_id;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    int id;
                    int.TryParse(reader["Id"].ToString(), out id);
                    double lat;
                    double.TryParse(reader["Coordinates_Latitude"].ToString(), out lat);
                    double lon;
                    double.TryParse(reader["Coordinates_Longitude"].ToString(), out lon);
                    marks.Add(new MapMark
                    {
                        Id = id,
                        Coordinates = new Point(lat, lon)
                    });

                }
                reader.Close();
            }
            catch (SqlException e)
            {
                string err = "";
                foreach (SqlError error in e.Errors)
                {
                    err += error.Number.ToString() + " ";
                }
                throw new ApplicationException(err);
            }
            finally
            {
                con.Close();
            }
            return marks;
        }
        [WebMethod]
        public void AddBrick()
        {
            //BrickDebugger bd = BrickDebugger.GetDebugger(Server.MapPath("~/log.txt"));
            string path = Server.MapPath("~/log.txt");
            string log = "";
            string msg = "";
        
            CultureInfo culture = new CultureInfo("en-US");
            culture.NumberFormat.NumberDecimalSeparator = ".";

            string request_lat = Context.Request.Params["lat"];
            double lat = double.Parse(request_lat, CultureInfo.InvariantCulture);

            double lon = double.Parse(Context.Request.Params["lon"], CultureInfo.InvariantCulture);

            string stamp = Context.Request.Params["stamp"];
            int numero = int.Parse(Context.Request.Params["numero"]);
            bool has_number = bool.Parse(Context.Request.Params["has_number"]);
            int quality = int.Parse(Context.Request.Params["quality"]);
            int privacy = int.Parse(Context.Request.Params["privacy"]);
            DateTime disc_date = DateTime.Parse(Context.Request.Params["disc_date"]).ToLocalTime();
            string img_path = Context.Request.Params["photo"];
            string comment = Context.Request.Params["comment"];

            log += "Request variables are read/n";

            int mark_id = 0;
            int user_id = BrickCookiesHelper.GetUserData().Id;

            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            log += "Created sql connection/n";
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("AddNewBrick", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @lat
                param.ParameterName = "@lat";
                param.SqlDbType = SqlDbType.Float;
                param.Value = lat;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @lon
                param.ParameterName = "@lon";
                param.SqlDbType = SqlDbType.Float;
                param.Value = lon;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @stamp
                param.ParameterName = "@stamp";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = stamp;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                if (has_number && int.Parse(Context.Request.Form["numero"]) != -1)
                {
                    param = new SqlParameter(); // @numero
                    param.ParameterName = "@numero";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = numero;
                    param.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(param);
                }

                param = new SqlParameter(); // @has_number
                param.ParameterName = "@has_number";
                param.SqlDbType = SqlDbType.Bit;
                param.Value = has_number;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @quality
                param.ParameterName = "@quality";
                param.SqlDbType = SqlDbType.Int;
                param.Value = quality;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @disc_date
                param.ParameterName = "@disc_date";
                param.SqlDbType = SqlDbType.DateTime;
                param.Value = disc_date;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @privacy
                param.ParameterName = "@privacy";
                param.SqlDbType = SqlDbType.Int;
                param.Value = privacy;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @comment
                param.ParameterName = "@comment";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = comment;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);
                
                param = new SqlParameter(); // @user_id
                param.ParameterName = "@user_id";
                param.SqlDbType = SqlDbType.Int;
                param.Value = user_id;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @img_path
                param.ParameterName = "@img_path";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = img_path;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                SqlParameter param_mark_id = new SqlParameter(); // @mark_id
                param_mark_id.ParameterName = "@mark_id";
                param_mark_id.SqlDbType = SqlDbType.Int;
                param_mark_id.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_mark_id);

                cmd.ExecuteNonQuery();
                log += "Executed query/n";
                mark_id = (int)param_mark_id.Value;
            }
            catch (SqlException e)
            {
                string err = "";
                foreach (SqlError error in e.Errors)
                {
                    err += error.Number.ToString() + " ";
                }
                log += err + "/n";
                throw new ApplicationException(err);
            }
            finally
            {
                con.Close();
                log += "Connection closed/n";
            }

            if (mark_id == 0)
            {
                Context.Response.Write(new JavaScriptSerializer().Serialize(new
                {
                    mark_id = mark_id,
                    status = false,
                    log = log,
                    msg = "Почему-то не удалось сохранить кирпичик, попробуйте попозже"
                }));
            }
            else
            {
                string imageUrl = "";
                if (img_path != "")
                    imageUrl = Context.Request.Url.GetLeftPart(UriPartial.Authority) +
                        img_path.Replace(Context.Request.ServerVariables["APPL_PHYSICAL_PATH"], "/").Replace(@"\", "/");
                Context.Response.Write(new JavaScriptSerializer().Serialize(new
                {
                    mark_id = mark_id,
                    status = true,
                    //path = path,
                    //log = log,
                    imageUrl = imageUrl,
                    msg = msg
                }));
            }
                

            return;
        }

        [WebMethod]
        public void SaveEditedBrick()
        {
            //BrickDebugger bd = BrickDebugger.GetDebugger(Server.MapPath("~/log.txt"));
            string path = Server.MapPath("~/log.txt");
            string log = "";
            string msg = "";

            CultureInfo culture = new CultureInfo("en-US");
            culture.NumberFormat.NumberDecimalSeparator = ".";

            string stamp = Context.Request.Params["stamp"];
            int numero = int.Parse(Context.Request.Params["numero"]);
            bool has_number = bool.Parse(Context.Request.Params["has_number"]);
            int quality = int.Parse(Context.Request.Params["quality"]);
            int privacy = int.Parse(Context.Request.Params["privacy"]);
            DateTime disc_date = DateTime.Parse(Context.Request.Params["disc_date"]).ToLocalTime();
            string img_path = Context.Request.Params["photo"];
            int brick_id = int.Parse(Context.Request.Params["brick_id"]);

            log += "Request variables are read/n";

            int user_id = BrickCookiesHelper.GetUserData().Id;

            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            log += "Created sql connection/n";
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SaveEditedBrick", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @stamp
                param.ParameterName = "@stamp";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = stamp;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                if (has_number && int.Parse(Context.Request.Form["numero"]) != -1)
                {
                    param = new SqlParameter(); // @numero
                    param.ParameterName = "@numero";
                    param.SqlDbType = SqlDbType.Int;
                    param.Value = numero;
                    param.Direction = ParameterDirection.Input;
                    cmd.Parameters.Add(param);
                }

                param = new SqlParameter(); // @has_number
                param.ParameterName = "@has_number";
                param.SqlDbType = SqlDbType.Bit;
                param.Value = has_number;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @quality
                param.ParameterName = "@quality";
                param.SqlDbType = SqlDbType.Int;
                param.Value = quality;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @disc_date
                param.ParameterName = "@disc_date";
                param.SqlDbType = SqlDbType.DateTime;
                param.Value = disc_date;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @privacy
                param.ParameterName = "@privacy";
                param.SqlDbType = SqlDbType.Int;
                param.Value = privacy;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @user_id
                param.ParameterName = "@user_id";
                param.SqlDbType = SqlDbType.Int;
                param.Value = user_id;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @new_img_path
                param.ParameterName = "@new_img_path";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = img_path;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @brick_id
                param.ParameterName = "@brick_id";
                param.SqlDbType = SqlDbType.Int;
                param.Value = brick_id;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();
                log += "Executed query/n";
            }
            catch (SqlException e)
            {
                string err = "";
                foreach (SqlError error in e.Errors)
                {
                    err += error.Number.ToString() + " ";
                }
                log += err + "/n";
                throw new ApplicationException(err);
            }
            finally
            {
                con.Close();
                log += "Connection closed/n";
            }

            return;
        }
        [WebMethod]
        public void SaveDraftPhoto()
        {
            string img_path = "";
            string msg = "";

            /*********************************** saving image ***********************************/
            // Установить заголовки (назад возвращаем JSON-данные о загруженном файле)
            this.Context.Response.ContentType = "application/json; charset=utf-8";
            //img_path = ImageHelper.SaveImageOnServer(Context.Request.Files["file"], out msg);

            // Ограничение на размер файла в 10 Мб
            const int __MaxSise = 10 * 1024;

            // Ограничения на тип файлов (рег. выражение),
            // в данном случае разрешается загружать только картинки JPG или PNG
            const string __FileType = @"(image/jp[e]?g)|(image/png)";
            try
            {
                // Получить файл
                HttpPostedFile file = Context.Request.Files["file"];

                if (file != null)
                {
                    // Проверить тип файла
                    if (Regex.IsMatch(file.ContentType, __FileType))
                    {
                        if (file.ContentLength / 1024 <= __MaxSise)
                        {
                            string extension = file.FileName.Substring(file.FileName.LastIndexOf('.') + 1);
                            img_path = String.Format("{0}_{1}", Guid.NewGuid(), DateTime.Now.ToString("yyyyMMdd"));
                            string img_small_path = img_path + "_small." + extension;
                            img_path = img_path + "." + extension;

                            string folderPath = "/uploads/" + img_path.Substring(0, 2) + "/" + img_path.Substring(2, 2) + "/";
                            if (!System.IO.Directory.Exists(Server.MapPath(folderPath)))
                            {
                                System.IO.Directory.CreateDirectory(Server.MapPath("~" + folderPath));
                            }
                            img_path = folderPath + img_path;
                            file.SaveAs(Server.MapPath("~" + img_path));

                            // Create and save small image:
                            System.Drawing.Image img_to_resize = System.Drawing.Image.FromStream(file.InputStream);

                            int width, heigth;
                            ImageHelper.GetDimensions(img_to_resize, out width, out heigth);
                            System.Drawing.Bitmap img_small = ImageHelper.ResizeImage(img_to_resize, width, heigth);

                            // Compressing image - from https://msdn.microsoft.com/en-us/library/bb882583(v=vs.110).aspx
                            ImageCodecInfo imgEncoder = ImageHelper.GetEncoder(extension);
                            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                            EncoderParameters myEncoderParameters = new EncoderParameters(1);
                            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 90L);
                            myEncoderParameters.Param[0] = myEncoderParameter;

                            img_small.Save(Server.MapPath("~" + folderPath + img_small_path), imgEncoder, myEncoderParameters);

                            /********************* Saving draft photo to DB ********************/
                            SqlConnection con = new SqlConnection(
                                WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
                            con.Open();
                            SqlCommand cmd = new SqlCommand("AddDraftPhoto", con);
                            cmd.CommandType = CommandType.StoredProcedure;

                            SqlParameter param = new SqlParameter(); // @img_path
                            param.ParameterName = "@img_path";
                            param.SqlDbType = SqlDbType.NVarChar;
                            param.Value = img_path;
                            param.Direction = ParameterDirection.Input;
                            cmd.Parameters.Add(param);

                            param = new SqlParameter(); // @upload_date
                            param.ParameterName = "@upload_date";
                            param.SqlDbType = SqlDbType.DateTime;
                            param.Value = DateTime.Now;
                            param.Direction = ParameterDirection.Input;
                            cmd.Parameters.Add(param);

                            cmd.ExecuteNonQuery();
                            /********************* End of saving draft photo to DB ********************/
                        }
                        else
                            msg += String.Format("Файл должен весить меньше {0} kB", __MaxSise);
                    }
                    else
                        msg += "Некорректный формат изображения, разрешается PNG или JPG";
                }
                else
                {
                    msg += "File is null";
                }
            }
            catch (Exception ex)
            {
                Context.Response.Write(new JavaScriptSerializer().Serialize(new
                {
                    status = false,
                    msg = ex.Message //"Ошибка на сервере."
                }));
                return;
            }
            Context.Response.Write(new JavaScriptSerializer().Serialize(new
            {
                status = true,
                imageUrl = Context.Request.Url.GetLeftPart(UriPartial.Authority) +
                    img_path.Replace(Context.Request.ServerVariables["APPL_PHYSICAL_PATH"], "/").Replace(@"\", "/"),
                msg = msg
            }));

            return;
        }

        [WebMethod]
        public void DropNotUsedPhoto()
        {
            BrickDebugger bd = BrickDebugger.GetDebugger(Server.MapPath("~" + "/log.txt"));
            List<string> photos_to_drop = new List<string>();
            
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetNotUsedPhotos", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    string img_path = reader["Path"].ToString();
                    photos_to_drop.Add(img_path);
                }
                reader.Close();

                cmd = new SqlCommand("RemoveNotUsedPhotos", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                string err = "";
                foreach (SqlError error in e.Errors)
                {
                    err += error.Number.ToString() + " ";
                }
                bd.WriteToLog(DateTime.Now.ToString() + err + "\n");
            }
            finally
            {
                con.Close();
            }

            foreach (var img_path in photos_to_drop)
            {
                try
                {
                    System.IO.File.Delete(Server.MapPath("~" + img_path));
                    Match match = Regex.Match(img_path, "[.]");
                    string small_img_path = img_path.Substring(0, match.Index) + "_small" + img_path.Substring(match.Index);
                    System.IO.File.Delete(Server.MapPath("~" + small_img_path));
                }
                catch (Exception ex)
                {
                    bd.WriteToLog(DateTime.Now.ToString() + ex.Message + "\n");
                }
            }
            return;
        }

        [WebMethod]
        public Brick GetBrickByMarkId(int markId)
        {
            //System.Threading.Thread.Sleep(300); 
            Brick brick = new Brick();

            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("GetBrickByMark", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @mark_id
                param.ParameterName = "@mark_id";
                param.SqlDbType = SqlDbType.Int;
                param.Value = markId;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                SqlParameter param_id = new SqlParameter(); // @id
                param_id.ParameterName = "@id";
                param_id.SqlDbType = SqlDbType.Int;
                param_id.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_id);

                SqlParameter param_stamp = new SqlParameter(); // @stamp
                param_stamp.ParameterName = "@stamp";
                param_stamp.SqlDbType = SqlDbType.NVarChar;
                param_stamp.Size = 53;
                param_stamp.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_stamp);

                SqlParameter param_has_number = new SqlParameter(); // @has_number
                param_has_number.ParameterName = "@has_number";
                param_has_number.SqlDbType = SqlDbType.Bit;
                param_has_number.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_has_number);

                SqlParameter param_number = new SqlParameter(); // @number
                param_number.ParameterName = "@number";
                param_number.SqlDbType = SqlDbType.Int;
                param_number.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_number);

                SqlParameter param_quality = new SqlParameter(); // @quality
                param_quality.ParameterName = "@quality";
                param_quality.SqlDbType = SqlDbType.Int;
                param_quality.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_quality);

                SqlParameter param_privacy = new SqlParameter(); // @privacy
                param_privacy.ParameterName = "@privacy";
                param_privacy.SqlDbType = SqlDbType.Int;
                param_privacy.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_privacy);

                SqlParameter param_comment = new SqlParameter(); // @comment
                param_comment.ParameterName = "@comment";
                param_comment.SqlDbType = SqlDbType.NVarChar;
                param_comment.Size = 512;
                param_comment.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_comment);

                SqlParameter param_discovery_date = new SqlParameter(); // @discovery_date 
                param_discovery_date.ParameterName = "@discovery_date";
                param_discovery_date.SqlDbType = SqlDbType.DateTime;
                param_discovery_date.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_discovery_date);

                SqlParameter param_user_id = new SqlParameter(); // @user_id
                param_user_id.ParameterName = "@user_id";
                param_user_id.SqlDbType = SqlDbType.Int;
                param_user_id.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_user_id);

                SqlParameter param_login = new SqlParameter(); // @login 
                param_login.ParameterName = "@login";
                param_login.SqlDbType = SqlDbType.NVarChar;
                param_login.Size = 53;
                param_login.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_login);

                SqlParameter param_photo = new SqlParameter(); // @photo
                param_photo.ParameterName = "@photo";
                param_photo.SqlDbType = SqlDbType.NVarChar;
                param_photo.Size = 256;
                param_photo.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(param_photo);

                cmd.ExecuteNonQuery();
                brick.Id = (int)param_id.Value;
                brick.Stamp = (string)param_stamp.Value;

                brick.HasNumber = (bool)param_has_number.Value;
                
                try{
                    brick.Number = Int32.Parse(param_number.Value.ToString());
                }
                catch(Exception e){
                    brick.Number  = null;
                }

                brick.Quality = (QualityStars)param_quality.Value;

                if (param_photo.Value == DBNull.Value)
                    brick.Photo = "";
                else
                    brick.Photo = (string)param_photo.Value;

                brick.Privacy = (PrivacyStatus)param_privacy.Value;
                brick.DiscoveryDate = (DateTime)param_discovery_date.Value;

                if (param_comment.Value == DBNull.Value)
                    brick.Comment = "";
                else
                    brick.Comment = (string)param_comment.Value;

                User user = new DB.User();
                user.Id = (int)param_user_id.Value;
                user.Login = (string)param_login.Value;

                brick.Mark = new MapMark() { Id = markId, Author = user };

                return brick;
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message + e.StackTrace);
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        public int DeleteMark(int markId)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("DeleteMark", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @mark_id
                param.ParameterName = "@mark_id";
                param.SqlDbType = SqlDbType.Int;
                param.Value = markId;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message + e.StackTrace);
            }
            finally
            {
                con.Close();
            }
            return markId;
        }

        [WebMethod]
        public void DeletePhoto(int brick_id, string photo)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("DeletePhoto", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param_id = new SqlParameter(); // @brick_id
                param_id.ParameterName = "@brick_id";
                param_id.SqlDbType = SqlDbType.Int;
                param_id.Value = brick_id;
                param_id.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param_id);
                
                SqlParameter param_photo = new SqlParameter(); // @photo
                param_photo.ParameterName = "@photo";
                param_photo.Value = photo;
                param_photo.SqlDbType = SqlDbType.NVarChar;
                param_photo.Size = 256;
                param_photo.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param_photo);

                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message + e.StackTrace);
            }
            finally
            {
                con.Close();
            }
            return;
        }

        [WebMethod]
        public int SaveNewMarkCoords(int markId, double lat, double lon)
        {
            SqlConnection con = new SqlConnection(
              WebConfigurationManager.ConnectionStrings["LocalSqlServer"].ConnectionString);

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SaveNewMarkCoords", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter(); // @mark_id
                param.ParameterName = "@mark_id";
                param.SqlDbType = SqlDbType.Int;
                param.Value = markId;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @lat
                param.ParameterName = "@lat";
                param.SqlDbType = SqlDbType.Float;
                param.Value = lat;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                param = new SqlParameter(); // @lon
                param.ParameterName = "@lon";
                param.SqlDbType = SqlDbType.Float;
                param.Value = lon;
                param.Direction = ParameterDirection.Input;
                cmd.Parameters.Add(param);

                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new ApplicationException(e.Message + e.StackTrace);
            }
            finally
            {
                con.Close();
            }
            return markId;
        }


    }

}
