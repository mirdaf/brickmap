﻿<%@ Page Title="BrickMap - Ваш профиль" Language="C#" MasterPageFile="~/BrickMaster.Master" AutoEventWireup="true"
    CodeBehind="Profile.aspx.cs" Inherits="BrickMap.Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="profileForm" class="regform">
        <div class="reg-row">
            <div class="reg-label">E-mail:</div>
            <asp:TextBox ID="txtEmail" runat="server" TextMode="Email" CssClass="regtext" />
        </div>
        <div class="reg-row">
            <div class="reg-label">Логин:</div>
            <asp:TextBox ID="txtLogin" runat="server" CssClass="regtext" />
        </div>
        <div class="reg-row">
            <div class="reg-label">Новый пароль:</div>
            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" CssClass="regtext" />
        </div>
        <div class="reg-row">
            <div class="reg-label">Еще раз пароль:</div>
            <asp:TextBox ID="txtPasswordDouble" runat="server" TextMode="Password" CssClass="regtext" />
            <asp:CompareValidator ID="ComparePasswordsValidator" runat="server" Type="String" Operator="Equal"
                ControlToCompare="txtPassword" ControlToValidate="txtPasswordDouble" ErrorMessage="Пароли не совпадают"
                ForeColor="Red" />
        </div>

        <asp:Label ID="txtErrorMessage" runat="server" CssClass="error-main" />
        <asp:Button ID="submitEditProfile" runat="server" CssClass="panel-btn light-text" Text="Сохранить изменения" /><br />
        <asp:Label ID="txtMessage" runat="server" />
    </div>
</asp:Content>
