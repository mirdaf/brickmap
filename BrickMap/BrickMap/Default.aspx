﻿<%@ Page Title="Brick Map" Language="C#" MasterPageFile="~/BrickMaster.Master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="BrickMap.Default" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Brick Map</title>
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.css" />
    <link rel="stylesheet" type="text/css" href="css/pickmeup.min.css" />
    <link rel="stylesheet" href="css/lightbox.min.css" />
    <link href="vendors/MarkerCluster/MarkerCluster.css" rel="stylesheet" />
    <link href="vendors/MarkerCluster/MarkerCluster.Default.css" rel="stylesheet" />
    <script src="Scripts/jquery.pickmeup.min.js"></script>
    <script src="Scripts/Brick.js"></script>
    <script src="Scripts/BrickPanel.js"></script>
    <script src="Scripts/BrickPanelControl.js"></script>
    <script src="Scripts/BrickMapOsm.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <Services>
            <asp:ServiceReference Path="~/MyDbService.asmx" />
            <asp:ServiceReference Path="~/LoginService.asmx" />
        </Services>
    </asp:ScriptManager>

    <input type="hidden" runat="server" id="mark_id" />

    <!-- Меню добавления нового кирпича -->
    <div class="addnew-menu-container">
        <a href="#" class="addnew-main" id="addnew-button" title="Добавить кирпич на карту">
            <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 72 72">
                <path fill="#cacfb4" d="M111.451,128.532H70.725V87.807c0-5.522-4.478-10-9.999-10c-5.523,0-10,4.477-10,10v40.725H10c-5.523,0-10,4.479-10,10.002
	c0,5.52,4.477,10,10,10h40.726v40.723c0,5.523,4.477,10.002,10,10.002c5.521,0,9.999-4.479,9.999-10.002v-40.723h40.726
	c5.521,0,9.999-4.48,9.999-10C121.45,133.011,116.972,128.532,111.451,128.532z"
                    transform="scale(0.5) translate(12,-69)" />
            </svg>
        </a>
        <div class="addnew-wrapper" id="addnew-wrapper">
            <ul>
                <li>
                    <a href="#" id="in-centre-mode" title="Добавить в центр">
                        <span class="addnew-centre">
                            <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 72 72">
                                <path d="M49.743,23.779c0.003-0.101,0.01-0.229,0.01-0.387C49.751,13.62,41.831,5.699,32.058,5.699
	c-9.773,0-17.693,7.921-17.693,17.694c0,0.152,0.005,0.279,0.008,0.381c-0.009,0.269-0.015,0.54-0.015,0.813
	c0.026,8.175,4.212,16.961,8.315,23.82c4.114,6.846,8.205,11.721,8.237,11.759c0.285,0.34,0.703,0.537,1.148,0.537
	c0.446,0,0.864-0.197,1.15-0.537c0.03-0.038,4.123-4.913,8.235-11.759c4.103-6.858,8.289-15.643,8.315-23.82
	C49.758,24.314,49.752,24.045,49.743,23.779z M38.872,46.862c-1.993,3.316-3.991,6.17-5.487,8.189
	c-0.508,0.685-0.957,1.271-1.327,1.748c-0.37-0.476-0.817-1.063-1.325-1.748c-4.515-6.023-13.442-19.737-13.375-30.465
	c0-0.247,0.004-0.492,0.014-0.736v-0.095c-0.004-0.157-0.008-0.277-0.008-0.362c0.014-8.116,6.58-14.68,14.693-14.693
	c8.116,0.014,14.68,6.578,14.696,14.693c0,0.083-0.006,0.203-0.01,0.362v0.089c0.008,0.249,0.014,0.495,0.014,0.742
	C46.783,31.717,42.87,40.239,38.872,46.862z M32.059,15.926c-3.706,0-6.708,3.004-6.71,6.708c0.002,3.706,3.004,6.708,6.71,6.71
	c3.706-0.002,6.708-3.004,6.708-6.71C38.767,18.93,35.764,15.926,32.059,15.926z M32.059,26.344c-2.049-0.004-3.706-1.661-3.71-3.71
	c0.004-2.047,1.661-3.704,3.71-3.708c2.047,0.004,3.706,1.661,3.708,3.708C35.764,24.683,34.105,26.34,32.059,26.344z"
                                    transform="scale(0.8) translate(2,0)" fill="#cacfb4" />
                            </svg>
                        </span>
                    </a>
                </li>
                <li><a href="#" id="by-location-mode" title="Определить мое местоположение и добавить кирпич"><span class="addnew-location">
                    <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 72 72">
                        <path d="M186.236,34.038c0-0.896-0.058-1.778-0.159-2.646l0,0c0,0,0-0.001,0-0.002c-1.063-9.084-7.524-16.513-16.095-18.997l0,0
	c0-0.001-0.002-0.001-0.003-0.001c-1.995-0.578-4.101-0.894-6.282-0.894c-0.896,0-1.777,0.058-2.646,0.159l0,0
	c0,0-0.002,0.001-0.003,0.001c-10.33,1.21-18.521,9.401-19.73,19.732c0,0.001,0,0.001,0,0.001l0,0
	c-0.102,0.869-0.159,1.75-0.159,2.646c0,0.896,0.058,1.777,0.159,2.646l0,0c0,0.001,0.001,0.002,0.001,0.003
	c0.146,1.244,0.393,2.457,0.732,3.63c0.001,0.003,0.001,0.006,0.002,0.009l0,0c2.145,7.391,7.966,13.213,15.356,15.356l0,0
	c0.001,0,0.002,0.001,0.003,0.001c1.176,0.341,2.391,0.589,3.639,0.734c0,0,0,0,0.001,0l0,0c0.868,0.102,1.75,0.16,2.646,0.16
	c2.182,0,4.289-0.316,6.285-0.895l0,0C179.372,52.96,186.236,44.303,186.236,34.038z M180.496,40.721
	c-1.639,0.729-3.543,1.35-5.639,1.844c0.434-2.67,0.68-5.531,0.68-8.528s-0.246-5.858-0.68-8.527c2.096,0.493,4,1.114,5.639,1.842
	c0.371,0.164,0.725,0.334,1.066,0.507c0.672,1.938,1.057,4.012,1.061,6.18c-0.004,2.166-0.389,4.239-1.061,6.178
	C181.22,40.388,180.868,40.557,180.496,40.721z M180.34,25.042c-1.774-0.701-3.754-1.293-5.898-1.75
	c-0.457-2.145-1.049-4.124-1.752-5.898C175.923,19.145,178.587,21.809,180.34,25.042z M173.455,34.037
	c0.002,3.183-0.281,6.212-0.779,8.978c-2.766,0.499-5.795,0.782-8.979,0.78c-3.182,0.002-6.213-0.281-8.979-0.78
	c-0.498-2.766-0.781-5.795-0.779-8.978c-0.002-3.183,0.281-6.212,0.779-8.978c2.766-0.498,5.797-0.781,8.979-0.781
	c3.184,0,6.213,0.283,8.979,0.781C173.173,27.825,173.457,30.854,173.455,34.037z M169.875,16.172
	c0.173,0.342,0.343,0.694,0.507,1.066c0.728,1.638,1.351,3.543,1.843,5.639c-2.668-0.434-5.529-0.68-8.527-0.682
	c-2.998,0.002-5.859,0.248-8.527,0.682c0.492-2.096,1.115-4.001,1.843-5.639c0.163-0.372,0.333-0.725,0.506-1.066
	c1.938-0.672,4.011-1.057,6.179-1.061C165.864,15.116,167.937,15.5,169.875,16.172z M154.701,17.395
	c-0.701,1.773-1.293,3.753-1.75,5.896c-2.144,0.457-4.122,1.048-5.896,1.75C148.806,21.81,151.47,19.147,154.701,17.395z
	 M152.537,42.565c-2.097-0.494-4.002-1.115-5.641-1.844c-0.371-0.163-0.723-0.332-1.063-0.506c-0.673-1.938-1.058-4.011-1.062-6.178
	c0.004-2.168,0.389-4.242,1.062-6.181c0.341-0.173,0.692-0.343,1.063-0.506c1.639-0.728,3.544-1.349,5.641-1.842
	c-0.434,2.669-0.68,5.53-0.682,8.527C151.857,37.034,152.103,39.895,152.537,42.565z M147.054,43.032
	c1.774,0.703,3.753,1.293,5.896,1.751c0.457,2.144,1.049,4.122,1.751,5.897C151.47,48.927,148.806,46.264,147.054,43.032z
	 M157.518,51.901c-0.173-0.34-0.343-0.693-0.506-1.064c-0.728-1.639-1.351-3.543-1.843-5.639c2.668,0.433,5.529,0.679,8.527,0.68
	c2.998-0.001,5.858-0.247,8.527-0.68c-0.493,2.096-1.115,4-1.843,5.639c-0.164,0.371-0.333,0.725-0.507,1.065
	c-1.938,0.672-4.01,1.057-6.178,1.061C161.529,52.958,159.457,52.574,157.518,51.901z M172.689,50.682
	c0.704-1.775,1.295-3.755,1.752-5.899c2.145-0.458,4.125-1.049,5.9-1.752C178.588,46.264,175.923,48.929,172.689,50.682z"
                            transform="scale(0.8) translate(-129,-1)" fill="#cacfb4" />
                    </svg>
                </span></a></li>
                <li><a href="#" id="by-coords-mode" title="Ввести координаты вручную"><span class="addnew-coords">
                    <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72" viewBox="0 0 72 72">
                        <path d="M118.22,28.502h-7.4c-0.824-10.342-9.059-18.579-19.401-19.401V1.699C91.419,0.762,90.66,0,89.722,0
	c-0.94,0-1.699,0.762-1.699,1.699v7.402C77.681,9.923,69.445,18.16,68.622,28.502H61.22c-0.939,0-1.699,0.76-1.699,1.698
	s0.76,1.698,1.699,1.698h7.401c0.823,10.343,9.059,18.578,19.401,19.401v7.401c0,0.938,0.759,1.699,1.699,1.699
	c0.938,0,1.697-0.761,1.697-1.699v-7.401c10.342-0.824,18.577-9.059,19.401-19.401h7.4c0.939,0,1.701-0.761,1.701-1.698
	S119.16,28.502,118.22,28.502z M107.417,28.502h-4.911c-0.761-5.769-5.319-10.328-11.087-11.087v-4.912
	C99.884,13.317,106.603,20.037,107.417,28.502z M88.023,31.898v7.651c-3.89-0.706-6.946-3.763-7.652-7.651H88.023z M80.371,28.502
	c0.707-3.889,3.763-6.945,7.652-7.651v7.651H80.371z M91.419,31.898h7.651c-0.706,3.888-3.765,6.944-7.651,7.65V31.898z
	 M91.419,28.502v-7.651c3.887,0.706,6.944,3.762,7.65,7.651H91.419z M88.023,12.502v4.913c-5.77,0.759-10.328,5.318-11.088,11.087
	h-4.912C72.837,20.036,79.557,13.316,88.023,12.502z M72.023,31.898h4.912c0.759,5.768,5.318,10.328,11.088,11.087v4.913
	C79.557,47.083,72.837,40.363,72.023,31.898z M91.419,47.898v-4.913c5.769-0.76,10.326-5.319,11.088-11.087h4.91
	C106.603,40.362,99.884,47.083,91.419,47.898z"
                            transform="scale(0.8) translate(-56,3)" fill="#cacfb4" />
                    </svg>
                </span></a></li>
            </ul>
        </div>
    </div>

    <div id="map_container" class="map">

        <div id="map" class="height">

            <!-- Brickpanel -->
            <div id="brickpanel" class="hidden">

                <!-- Preloader -->
                <div class="panelmodal-preloader-overlay">
                    <div class="panelmodal-preloader-container">
                        <div class="panelmodal-preloader-inner">
                            <div class="panelmodal-preloader shown">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End of preloader -->

                <p class="in-panel">
                    <label>Just another brick on the map ;)</label>
                </p>
                <img id="close_panel" src="Images/close.png" />
                <p class="in-panel">
                    <span id="photo-container" class="photo-container">
                        <a id="delete-photo">
                            Delete photo
                        </a>
                        <a id="photo_link">
                            <img id="brick_photo" src="about:blanc" class="big-photo" />
                        </a>
                    </span>
                </p>

                <div class="horiz-centered in-panel photo-loader hidden" id="photoPreloader"></div>

                <p id="uploader_paragraph" class="in-panel">
                    <span class="for-author-only align-center">
                        <label id="uploader-container" class="uploader-alone">
                            <input type="file" id="fileUploader" accept="image/*,image/jpeg" />
                            <span>Загрузить фотографию</span>
                        </label>
                    </span>
                </p>
                <p id="author_name" class="in-panel invisible">
                    <label>Автор:</label>
                    <input type="text" id="txtAuthor" class="half" readonly="" />
                </p>
                <p class="in-panel">
                    <label>Клеймо:</label>
                    <input type="text" id="txtStamp" class="half" />
                </p>
                <p class="in-panel">
                    <label>С номером:</label>
                    <label for="checkboxHasNumber" id="lblForHasNumber">
                        <input type="checkbox" id="checkboxHasNumber" />
                    </label>
                    <input type="text" id="txtNumber" class="third depend-on-checkbox-has-number" />
                </p>
                <p class="in-panel">
                    <label>Состояние:</label>
                    <a class="star unselected"></a>
                    <a class="star unselected"></a>
                    <a class="star unselected"></a>
                    <a class="star unselected"></a>
                    <a class="star unselected"></a>
                </p>
                <p class="in-panel">
                    <label>Найден:</label>
                    <input type="text" id="inputDate" class="half" />
                </p>
                <p class="for-author-only in-panel">
                    <label>Показывать:</label>
                    <select id="selectPrivacy" size="1">
                        <option value="0">только мне</option>
                        <option value="1">друзьям</option>
                        <option value="2">зарегистрированным</option>
                        <option value="3">всем</option>
                    </select>
                </p>
                <p class="in-panel">
                    <label>Комментарий:</label>
                    <textarea id="txtComment" class="half"></textarea>
                    <%--<div id="hiddenComment" class="hidden-comment"></div>--%>
                </p>

                <div id="brickpanel_common" class="invisible for-author-only in-panel">
                    <a href="#" id="btnEdit" class="panel-btn" title="Редактировать"><span>Редактировать</span></a>
                    <a href="#" id="btnMove" class="panel-btn" title="Переместить"><span>Переместить</span></a>
                    <a href="#" id="btnDelete" class="panel-btn" title="Удалить"><span>Удалить</span></a>
                </div>

                <div id="brickpanel_new" class="invisible for-author-only in-panel">
                    <a href="#" id="btnSaveNewBrick" class="panel-btn" title="Сохранить"><span>Сохранить</span></a>
                    <a href="#" id="btnCancel" class="panel-btn" title="Отменить"><span>Отменить</span></a>
                </div>

                <div id="brickpanel_edit" class="invisible for-author-only in-panel">
                    <a href="#" id="btnSaveEditedBrick" class="panel-btn" title="Сохранить"><span>Сохранить</span></a>
                    <a href="#" id="btnCancelEditing" class="panel-btn" title="Отменить"><span>Отменить</span></a>
                </div>

                <p class="in-panel">
                    <span id="link-container">
                        <label>Прямая ссылка:</label>
                        <input type="text" id="txtLink" class="half" readonly="readonly" />
                    </span>
                </p>
            </div>
            <!-- End of brickpanel -->

        </div>
    </div>

</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="bottomContent" runat="server">
    <script src="Scripts/lightbox.min.js"></script>
    <script>
        lightbox.option({
            'wrapAround': true,
            'positionFromTop': 70
        })
    </script>
    <script src="Scripts/AddNewBrickMenu.js"></script>
    <script src="Scripts/polyfills.js"></script>

</asp:Content>
