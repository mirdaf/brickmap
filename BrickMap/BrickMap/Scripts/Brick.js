﻿var BrickPanelControls = {
    btnSaveNewBrick : null,
    btnCancel : null,
    panel : null,
    stars : null,
    btnEdit : null,
    btnMove : null,
    btnDelete: null,
    photoUploader: null,
    deletePhoto: null,

    Init: function () {
        this.btnSaveNewBrick = document.getElementById("btnSaveNewBrick");
        this.btnCancel = document.getElementById("btnCancel");
        this.panel = document.getElementById("brickpanel");
        this.stars = document.getElementsByClassName("star");
        this.btnEdit = document.getElementById("btnEdit");
        this.btnMove = document.getElementById("btnMove");
        this.btnDelete = document.getElementById("btnDelete");
        this.photoUploader = document.getElementById("fileUploader");
        this.deletePhoto = document.getElementById("delete-photo");
        this.photoPreloader = document.getElementById("photoPreloader");
        this.link_container = document.getElementById("link-container");
        this.btnSaveEditedBrick = document.getElementById("btnSaveEditedBrick");
        this.btnCancelEditing = document.getElementById("btnCancelEditing");
    }
};

var Brick = function (stamp, number, hasNumber, quality, discoveryDate, privacy, mark, photo, latlng, comment) {
    this.Id = null;
    this.Stamp = stamp || "";
    this.Number = number || "";
    this.HasNumber = hasNumber || 0;
    this.Quality = quality || 3;
    this.DiscoveryDate = discoveryDate || new Date();
    this.Privacy = privacy || 0;
    this.Mark = mark;
    //this.Photos = photo || []; // for future
    this.Photo = photo || "";
    this.LatLng = latlng;
    this.Comment = comment;

    /* State of brick panel: new, common, or edit */
    this.brickState = null;
    this.SavingDraft = this.SaveAsDraft.bind(this);
}

Brick.prototype.SetLatLng = function (latlng) {
    this.LatLng = latlng;
}

Brick.prototype.SetBrickId = function (brick_id) {
    this.Id = brick_id;
}

Brick.prototype.FillPanel = function () {
    console.log('============================================================');
    console.log('FillPanel:');
    console.log(this);
    var el = document.getElementById("txtAuthor");
    if (this.Mark) { // for existing brick
        var profile = document.getElementById("profile");
        el.value = this.Mark.login;
        if (this.Mark.login == profile.innerText) {
            $(".for-author-only").show();
        }
        else {
            $(".for-author-only").hide();
        }
        this.ShowLink();
    }

    el = document.getElementById("txtStamp");
    el.value = this.Stamp;
    
    el = document.getElementById("checkboxHasNumber");
    el.checked = this.HasNumber;
    
    if(this.HasNumber)
        SetCheckedMode();
    else
        SetUncheckedMode();

    el = document.getElementById("txtNumber");
    if (this.Number == -1)
        el.value = "";
    else
        el.value = this.Number;
    this.ShowQualityRating();
    this.ShowPhotos();
    this.CleanFileUploader();

    $('#inputDate').pickmeup('set_date', this.DiscoveryDate);
    el = document.getElementById("selectPrivacy");
    el.value = this.Privacy;
    el = document.getElementById("txtComment");
    el.value = this.Comment || "";
}

Brick.prototype.ShowQualityRating = function () {
    var rate = this.Quality;
    var stars = document.getElementsByClassName("star");
    var len = stars.length;
    var i;
    for (i = 0; i < len; ++i) {
        if (i < rate) {
            stars[i].classList.remove("unselected");
            stars[i].classList.add("selected");
        }
        else {
            stars[i].classList.remove("selected");
            stars[i].classList.add("unselected");
        }
    }
}

Brick.prototype.ShowPhotos = function () {
    console.log("ShowPhotos");
    var brick_photo = document.getElementById("brick_photo");
    var photo_link = document.getElementById("photo_link");

    var container = document.getElementById("photo-container");
    var uploader = document.getElementById("uploader-container");
    brick_photo.src = this.Photo;

    if (!this.Photo || this.Photo == "") {
        container.classList.remove("shown");
        container.classList.add("hidden");
        //uploader.classList.remove("hidden");
    }
    else {
        photo_link.setAttribute("data-lightbox", this.Id);
        photo_link.href = this.Photo;

        var dot_position = this.Photo.lastIndexOf(".");
        var small_photo = this.Photo.substring(0, dot_position) + "_small" + this.Photo.substring(dot_position);
        brick_photo.src = small_photo;

        container.classList.remove("hidden");
        container.classList.add("shown");
        //uploader.classList.add("hidden");
    }
}

Brick.prototype.CleanFileUploader = function () {
    if (!BrickPanelControls.photoUploader.value)
        return;
    BrickPanelControls.photoUploader.setAttribute("type", "input");
    BrickPanelControls.photoUploader.setAttribute("type", "file");
}

Brick.prototype.ShowLink = function () {
    var brick_link = document.getElementById("txtLink");
    brick_link.value = "http://brickmap.ru/Default.aspx?brick=" + this.Mark.id;
    brick_link.onfocus = function () {
        brick_link.select();
    };
    brick_link.onblur = function () {
        brick_link.select(false);
    };
}

Brick.prototype.AddEventListeners = function (marker_state) {
    this.brickState = marker_state;
    if (this.brickState == MarkerStates.NEW) {
        BrickPanelControls.btnSaveNewBrick.onclick = this.AddBrickToDb.bind(this);
        BrickPanelControls.btnCancel.onclick = HideAddingNewBrickDialog;
        BrickPanelControls.panel.addEventListener("blur", this.SavingDraft, true); // why true?

        BrickPanelControls.link_container.classList.add("invisible");

        document.body.onkeydown = this.ListenToKeypad.bind(this);
        
        for (var i = 0; i < BrickPanelControls.stars.length; ++i)
            BrickPanelControls.stars[i].onclick = this.SetQuality.bind(this);

        BrickPanelControls.photoUploader.onchange = this.SaveDraftPhoto.bind(this);
    }
    else if (this.brickState == MarkerStates.COMMON) {
        BrickPanelControls.deletePhoto.onclick = null;
        BrickPanelControls.panel.removeEventListener("blur", this.SavingDraft);
        BrickPanelControls.btnEdit.onclick = this.SetEditBrickMode.bind(this);

        BrickPanelControls.btnMove.onclick = this.LetMoveMark.bind(this);
        BrickPanelControls.btnDelete.onclick = this.DeleteMark.bind(this);

        BrickPanelControls.link_container.classList.remove("invisible");

        for (var i = 0; i < BrickPanelControls.stars.length; ++i)
            BrickPanelControls.stars[i].onclick = null;
    }
    else if (this.brickState == MarkerStates.EDIT) {
        BrickPanelControls.panel.removeEventListener("blur", this.SavingDraft);
        BrickPanelControls.photoUploader.onchange = this.SaveDraftPhoto.bind(this);
        BrickPanelControls.deletePhoto.onclick = this.DeletePhoto.bind(this);
        BrickPanelControls.btnSaveEditedBrick.onclick = this.SaveEditedBrick.bind(this);
        BrickPanelControls.btnCancelEditing.onclick = this.SetCommonBrickMode.bind(this);

        for (var i = 0; i < BrickPanelControls.stars.length; ++i)
            BrickPanelControls.stars[i].onclick = this.SetQuality.bind(this);
    }
}

Brick.prototype.FreezeEventsHandlers = function () {
    BrickPanelControls.btnSaveNewBrick.onclick = null;
    BrickPanelControls.panel.removeEventListener("blur", this.SavingDraft);
    document.body.onkeydown = null;

    for (var i = 0; i < BrickPanelControls.stars.length; ++i)
        BrickPanelControls.stars[i].onclick = null;

    BrickPanelControls.photoUploader.onchange = null;
}

Brick.prototype.SetEditBrickMode = function () {
    var gBrickPanel = new BrickPanel();
    gBrickPanel.SetState(MarkerStates.EDIT);
    this.AddEventListeners(MarkerStates.EDIT);
}

Brick.prototype.SetCommonBrickMode = function () {
    var gBrickPanel = new BrickPanel();
    gBrickPanel.SetState(MarkerStates.COMMON);
    this.AddEventListeners(MarkerStates.COMMON);
    this.FillPanel();
}

Brick.prototype.SetQuality = function (e) {
    var selected_star = e.target;
    this.Quality = selected_star.getAttribute("q-rating");
    this.ShowQualityRating();
}

Brick.prototype.SetDate = function () {
    if (this.DiscoveryDate)
        $('#inputDate').pickmeup('set_date', this.DiscoveryDate);
}

Brick.prototype.ListenToKeypad = function (e) {
    if (e.keyCode == 27) { // esc
        this.SaveAsDraft();
    }
}

Brick.prototype.SaveAsDraft = function () {
    if (!UserIsLoggedIn()) 
        return;
    
    if (!marker) // save draft only when creating new brick
        return;

    console.log("Saving draft...");
    this.Stamp = L.DomUtil.get("txtStamp").value;

    this.HasNumber = L.DomUtil.get("checkboxHasNumber").checked;

    var number = L.DomUtil.get("txtNumber").value;
    this.Number = parseInt(number);
    if (isNaN(this.Number)) { // Строка преобразовалась в NaN. Не число
        this.Number = "";
    }

    this.DiscoveryDate = $('#inputDate').pickmeup('get_date', false);
    this.Privacy = L.DomUtil.get("selectPrivacy").value;
    this.LatLng = marker.getLatLng(); // global marker variable
    this.Comment = L.DomUtil.get("txtComment").value;
}

Brick.prototype.SaveDraftPhoto = function () {
    if (!UserIsLoggedIn())
        return;

    //if (!marker) // save draft only when creating new brick // to remove?
    //    return;

    console.log("Saving draft photo...");
    var formData = new FormData();
    var file = BrickPanelControls.photoUploader.files[0];

    if (file !== undefined) {
        formData.append("file", file);
        console.log('Файл присоединен');
    }
    else
        console.log('Файл не выбран');

    $.ajax({
        url: '/MyDbService.asmx/SaveDraftPhoto',
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        type: "post",
        beforeSend: this.ShowPhotoPreloader.bind(this),
        success: this.ShowDraftPhoto.bind(this),
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(jqXHR);
        }
    });
}

Brick.prototype.DeletePhoto = function () {
    this.ShowPhotoPreloader();
    BrickMap.MyDbService.DeletePhoto(this.Mark.id, this.Photo, this.ShowRestorePhotoButton.bind(this), OnError);
}

Brick.prototype.ShowRestorePhotoButton = function () {
    this.Photo = "";
    this.ShowPhotos();
    this.HidePhotoPreloader();
}

Brick.prototype.ShowPhotoPreloader = function () {
    console.log('Загрузка файла...');
    this.photoPreloader = document.getElementById("photoPreloader");
    this.photoPreloader.classList.remove("hidden");
    this.photoPreloader.classList.add("shown");
}

Brick.prototype.HidePhotoPreloader = function () {
    this.photoPreloader = document.getElementById("photoPreloader");
    this.photoPreloader.classList.remove("shown");
    this.photoPreloader.classList.add("hidden");
}

Brick.prototype.ShowDraftPhoto = function (e) {
    this.CleanFileUploader();
    this.HidePhotoPreloader();

    if (e.status) {
        console.log("Saved photo");
        var response = e;
        //this.Photos.push(response.imageUrl); // for future
        var photo_path = response.imageUrl;
        var path_array = photo_path.split("/uploads/");
        this.Photo = "/uploads/" + path_array[1];
        this.ShowPhotos();
        this.SaveAsDraft();
    }
    else {
        console.log("error: " + e.msg);
        ShowNotification("Error Uploading File");
    }
}

Brick.prototype.ShowNewBrickMarkOnMap = function (e) {
    console.log("Saved brick");

    this.brickState = MarkerStates.COMMON;
    this.AddEventListeners(this.brickState);

    this.CleanFileUploader();
    
    if (e.status) {
        var response = e;
        console.log(response);

        this.Mark = marker;
        this.Mark.id = response.mark_id;
        marks[this.Mark.id] = this.Mark;
        
        //this.Photos.push(response.imageUrl); // for future
        this.Photo = response.imageUrl; 
        this.ShowPhotos();

        NullifyMarker();

        var profile = document.getElementById("profile");
        this.Mark.login = profile.innerText;
        this.Mark.SetCommonMarkState();
        this.Mark.options.riseOnHover = true;
        this.Mark.options.riseOffset = 250;
        this.Mark.ShowMessage("Saved brick");
        this.Mark.dragging.disable();
        this.Mark.update();

        this.AddToCluster();

        this.FillPanel();
    }
    else {
        console.log("error: " + e.msg);
        ShowNotification("Error Uploading File");
    }
    this.ShowLink();
    console.log(this);

    HideBrickPanel();
}


Brick.prototype.AddToCluster = function () {
    cluster.addLayer(this.Mark);
    //this.Mark.onRemove(map); // ohh..
    //if()

}

Brick.prototype.LetMoveMark = function () {
    if (!UserIsLoggedIn()) 
        return;

    this.Mark.dragging.enable();
    this.Mark.ShowMessage("Меня можно таскать!");
    HideBrickPanel(false);
    this.Mark.addEventListener("dragend", this.ShowSavePlaceOrNotDialog.bind(this), false);
}

Brick.prototype.ShowSavePlaceOrNotDialog = function (e) {
    var panel_container = L.DomUtil.create('div', 'dialog-popup');

    var btnSavePlace = L.DomUtil.create('a', '', panel_container);
    btnSavePlace.id = "btnSavePlace";
    btnSavePlace.href = "#";
    btnSavePlace.title = "Запомнить новое местоположение";
    btnSavePlace.style.width = "auto";
    btnSavePlace.className = "panel-btn";
    btnSavePlace.innerHTML = "<span>Сохранить</span>";
    btnSavePlace.addEventListener("click", this.SaveNewCoords.bind(this), false);

    var btnKeepDragging = L.DomUtil.create('a', '', panel_container);
    btnKeepDragging.id = "btnKeepDragging";
    btnKeepDragging.href = "#";
    btnKeepDragging.title = "Подвигать еще";
    btnKeepDragging.style.width = "auto";
    btnKeepDragging.className = "panel-btn";
    btnKeepDragging.innerHTML = "<span>Двигать еще</span>";
    btnKeepDragging.addEventListener("click", KeepDragging);

    var btnCancelDragging = L.DomUtil.create('a', '', panel_container);
    btnCancelDragging.id = "btnCancelDragging";
    btnCancelDragging.href = "#";
    btnCancelDragging.title = "Вернуть на прежнее место";
    btnCancelDragging.style.width = "auto";
    btnCancelDragging.className = "panel-btn";
    btnCancelDragging.innerHTML = "<span>Отменить</span>";
    btnCancelDragging.addEventListener("click", this.CancelDragging.bind(this), false);

    var dialog = L.popup()
        .setLatLng(marks[this.Mark.id].getLatLng())
        .setContent(panel_container)
        .openOn(map);
}

Brick.prototype.SaveNewCoords = function (e) {
    map.closePopup();
    this.LatLng = this.Mark.getLatLng();
    BrickMap.MyDbService.SaveNewMarkCoords(this.Mark.id, this.LatLng.lat, this.LatLng.lng,
        function () { map.closePopup(); }, OnError);
    this.Mark.dragging.disable();
    this.Mark.update();
}

function KeepDragging() {
    map.closePopup();
}

Brick.prototype.CancelDragging = function (e) {
    map.closePopup();
    this.Mark.setLatLng(this.LatLng);
    this.Mark.dragging.disable();
    this.Mark.update();
}

Brick.prototype.DeleteMark = function () {
    if (!UserIsLoggedIn()) 
        ShowNotification("Авторизуйтесь, чтобы удалять метки");

    ShowModalDialog("Удалить кирпич?", "да", "нет")
      .then(
        result => this.DoDeleteMark(),
        error => CloseModalDialog
      );
}

Brick.prototype.DoDeleteMark = function () {
    ShowPreloader();
    BrickMap.MyDbService.DeleteMark(this.Id, RemoveMarkFromMap, OnError);
}

Brick.prototype.AddBrickToDb = function (e) {
    this.FreezeEventsHandlers();

    if (!UserIsLoggedIn()) 
        ShowNotification("Авторизуйтесь, чтобы добавлять метки");;

    // Отключить отправку формы
    e.preventDefault();
    
    this.Stamp = L.DomUtil.get("txtStamp").value;

    var number = L.DomUtil.get("txtNumber").value;
    this.Number = parseInt(number);

    if (isNaN(this.Number)) { // Строка преобразовалась в NaN. Не число
        this.Number = -1;
    }

    this.DiscoveryDate = $('#inputDate').pickmeup('get_date', "d-m-Y");
    this.Privacy = L.DomUtil.get("selectPrivacy").value;
    this.LatLng = marker.getLatLng(); // global marker variable

    console.log("Adding brick: ");
    console.log(this);

    var formData = new FormData();
    formData.append('lat', this.LatLng.lat);
    formData.append("lon", this.LatLng.lng);
    formData.append("stamp", this.Stamp);
    formData.append("numero", this.Number);
    formData.append("has_number", this.HasNumber);
    formData.append("quality", this.Quality);
    formData.append("disc_date", this.DiscoveryDate.toString());
    formData.append("privacy", this.Privacy);
    formData.append("comment", this.Comment);
    formData.append("photo", this.Photo);

    $.ajax({
        url: '/MyDbService.asmx/AddBrick',
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        type: "post",
        beforeSend: ShowPreloader,
        success: this.ShowNewBrickMarkOnMap.bind(this),
        error: function (jqXHR, textStatus, errorMessage) {
            // here must be setting back events listeners
            console.log(jqXHR);
            ShowNotification(errorMessage);
        },
        complete: HidePreloader
    });

}

Brick.prototype.SaveEditedBrick = function (e) {
    this.FreezeEventsHandlers();

    if (!UserIsLoggedIn()) 
        ShowNotification("Авторизуйтесь, чтобы добавлять метки");;
    
    // Отключить отправку формы
    e.preventDefault();
    
    this.Stamp = L.DomUtil.get("txtStamp").value;

    this.HasNumber = L.DomUtil.get("checkboxHasNumber").checked;
    
    var number = L.DomUtil.get("txtNumber").value;
    this.Number = parseInt(number);
    if (isNaN(this.Number)) { // Строка преобразовалась в NaN. Не число
        this.Number = -1;
    }

    this.DiscoveryDate = $('#inputDate').pickmeup('get_date', "d-m-Y");
    this.Privacy = L.DomUtil.get("selectPrivacy").value;
    //this.LatLng = marker.getLatLng(); // global marker variable

    console.log("Saving edited brick: ");
    console.log(this);

    var formData = new FormData();
    formData.append("stamp", this.Stamp);
    formData.append("numero", this.Number);
    formData.append("has_number", this.HasNumber);
    formData.append("quality", this.Quality);
    formData.append("disc_date", this.DiscoveryDate.toString());
    formData.append("privacy", this.Privacy);
    formData.append("photo", this.Photo);
    formData.append('brick_id', this.Id);

    $.ajax({
        url: '/MyDbService.asmx/SaveEditedBrick',
        data: formData,
        contentType: false,
        processData: false,
        dataType: "json",
        type: "post",
        beforeSend: function () {
            console.log('Загрузка...');
        },
        success: this.ShowChangesSaved.bind(this),
        error: function (jqXHR, textStatus, errorMessage) {
            console.log(jqXHR);
        }
    });

}

Brick.prototype.ShowChangesSaved = function () {
    ShowNotification("Changes are saved");
    var gBrickPanel = new BrickPanel();
    gBrickPanel.SetState(MarkerStates.COMMON);
    this.AddEventListeners(MarkerStates.COMMON);
}
