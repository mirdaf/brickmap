﻿brickMark = function (lat_lng, options, mark_id, exist) {
    return new BrickMark(lat_lng, options, mark_id, exist);
};

var BrickMark = L.Marker.extend({

    initialize: function (lat_lng, options, mark_id, exist) {
        L.Marker.prototype.initialize.call(this, lat_lng, options);

        this.id = mark_id;
        this.exist = exist;
        this.brick = null;
        this.user_id = null;
        this.login = null;

        // add new brick marker to the map
        if (!this.exist)
            this.addTo(map);

        this.addEventListener("click", this.ShowBrickPanel.bind(this), false);
    },
    statics: {
        selected_mark_id: null,
    },

});

BrickMark.prototype.SetBrick = function (brick) {
    this.brick = brick;
}

BrickMark.prototype.ShowBrickPanel = function () {
    if (this.exist)
        this.TryShowCommonBrickPanel();
    else
        this.ShowNewBrickPanel();
}

BrickMark.prototype.TryShowCommonBrickPanel = function () {
    this.SetSelectedMarkState();
    if (this.brick) {
        this.ShowCommonBrickPanel();
    }
    else {
        ShowPreloader();
        this.GetBrickInfo();
    }
}

BrickMark.prototype.ShowCommonBrickPanel = function () {
    gBrickPanel.SetState(MarkerStates.COMMON);
    this.brick.FillPanel();
    this.brick.AddEventListeners(MarkerStates.COMMON);
    EnableDatePicker();
    HidePreloader();
    ShowBrickPanel();
}

BrickMark.prototype.GetBrickInfo = function () {
    BrickMap.MyDbService.GetBrickByMarkId(this.id,
        this.SetBrickInfo.bind(this), OnError);
}

BrickMark.prototype.SetBrickInfo = function (brick) {
    console.log(brick);

    var number = (brick.HasNumber) ? brick.Number : "";

    this.user_id = brick.Mark.Author.Id;
    this.login = brick.Mark.Author.Login;

    var brick_object = new Brick(brick.Stamp, number, brick.HasNumber, brick.Quality,
        brick.DiscoveryDate, brick.Privacy, this, brick.Photo, null, brick.Comment);

    this.brick = brick_object;
    this.brick.SetLatLng(this.getLatLng());
    this.brick.SetBrickId(brick.Id);

    this.ShowCommonBrickPanel();
}

BrickMark.prototype.SetSelectedMarkState = function () {
    if (BrickMark.selected_mark_id && BrickMark.selected_mark_id != this.id) {
        marks[BrickMark.selected_mark_id].RemoveSelectedMarkState();
    }
    this.setIcon(selected_icon);
    this.setZIndexOffset(1000);
    BrickMark.selected_mark_id = this.id;
}

BrickMark.prototype.RemoveSelectedMarkState = function(){
    this.setIcon(common_icon);
    this.setZIndexOffset(0);
    BrickMark.selected_mark_id = null;
}

BrickMark.prototype.SetCommonMarkState = function () {
    this.exist = true;
    this.setIcon(common_icon);
    this.setZIndexOffset(0);
}

BrickMark.prototype.ShowNewBrickPanel = function(){
    gBrickPanel.SetState(MarkerStates.NEW);
    this.brick.FillPanel();
    this.brick.AddEventListeners(MarkerStates.NEW);
    this.ShowMessage("Меня можно таскать!");
    ShowBrickPanel();
}

BrickMark.prototype.ShowMessage = function(message){
    this.bindPopup(message);
    this.openPopup();
    //this.addEventListener("popupclose", this.HideMessage.bind(this), false);
    this.addEventListener("mouseover", this.HideMessage.bind(this), false);
}

BrickMark.prototype.HideMessage = function () {
    this.closePopup();
    this.unbindPopup();
    this.removeEventListener("mouseover", this.HideMessage, false);
    this.update();
}
