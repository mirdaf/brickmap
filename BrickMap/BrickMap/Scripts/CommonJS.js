﻿function ShowNotification(message, to_focus_after) {
    var label_for_msg = document.getElementById("modal-msg");
    label_for_msg.innerText = message;

    var overlay = $(".modal-overlay");
    overlay.css('display', 'block');
    overlay.click(function (event) {
        e = event || window.event;
        if (e.target == this) {
            $(overlay).css('display', 'none');
            $(to_focus_after).focus();
        }
    });
    var modal_window = $(".modal-window");
    modal_window.click(function (event) {
        $(overlay).css('display', 'none');
        $(to_focus_after).focus();
    });
}

function ShowModalDialog(message, yes_msg, no_msg) {

    return new Promise(function (resolve, reject) {

        var label_for_msg = document.getElementById("modal-dialog-msg");
        label_for_msg.innerText = message;

        var yes = $("#modal-yes");
        yes.text(yes_msg);
        yes.click(function (e) {
            yes.off("click");
            no.off("click");
            $(".modal-dialog-overlay").css('display', 'none');
            resolve();
        });

        var no = $("#modal-no");
        no.text(no_msg);
        no.click(function (e) {
            yes.off("click");
            no.off("click");
            $(".modal-dialog-overlay").css('display', 'none');
            reject();
        });
        $(".modal-dialog-overlay").css('display', 'block');
    });

}

function CloseModalDialog() {
    var overlay = $(".modal-dialog-overlay");
    overlay.css('display', 'none');
}

function ShowPreloader() {
    $(".modal-preloader-overlay").addClass('shown');
}

function HidePreloader() {
    $(".modal-preloader-overlay").removeClass('shown');
}

function ShowPanelPreloader() {
    $(".modal-preloader-overlay").addClass('shown');
    var brickpanel = document.getElementById("brickpanel");
    //brickpanel.onscroll = function () {
    //    console.log("событие onscrolllllllllllllllllllllllllllll");
    //    return false;
    //};
}

function HidePanelPreloader() {
    $(".modal-preloader-overlay").removeClass('shown');
    var panelmodal = document.getElementById("brickpanel");
    panelmodal.onscroll = function () {
        return true;
    };
}

var mixin = function (targetConstructor, mix) {
    for (var i in mix) {
        if (mix.hasOwnProperty(i)) {
            targetConstructor.prototype[i] = mix[i];
        }
    }
};


/**
 * From: https://learn.javascript.ru/mixins
*/
var eventMixin = {

    /**
     * Подписка на событие
     * Использование:
     *  menu.on('select', function(item) { ... }
    */
    on: function (eventName, handler) {
        if (!this._eventHandlers) this._eventHandlers = {};
        if (!this._eventHandlers[eventName]) {
            this._eventHandlers[eventName] = [];
        }
        this._eventHandlers[eventName].push(handler);
    },

    /**
     * Прекращение подписки
     *  menu.off('select',  handler)
     */
    off: function (eventName, handler) {
        var handlers = this._eventHandlers && this._eventHandlers[eventName];
        if (!handlers) return;
        for (var i = 0; i < handlers.length; i++) {
            if (handlers[i] == handler) {
                handlers.splice(i--, 1);
            }
        }
    },

    /**
     * Генерация события с передачей данных
     *  this.trigger('select', item);
     */
    trigger: function (eventName /*, ... */) {

        if (!this._eventHandlers || !this._eventHandlers[eventName]) {
            return; // обработчиков для события нет
        }

        // вызвать обработчики
        var handlers = this._eventHandlers[eventName];
        for (var i = 0; i < handlers.length; i++) {
            handlers[i].apply(this, [].slice.call(arguments, 1));
        }

    }
};