﻿var BrickPanel = function () {
    this.panel = document.getElementById("brickpanel");
    this.isOpen = false;
}

BrickPanel.prototype.SetState = function (marker_state) {
    var author_name = document.getElementById("author_name");
    var photo_uploader = document.getElementById("uploader_paragraph");
    var brickpanel_common = document.getElementById("brickpanel_common");
    var brickpanel_new = document.getElementById("brickpanel_new");
    var brickpanel_edit = document.getElementById("brickpanel_edit");

    switch (marker_state) {

        case MarkerStates.NEW:
            author_name.className = "invisible in-panel";
            photo_uploader.className = "visible in-panel";
            brickpanel_common.className = "invisible for-author-only in-panel";
            brickpanel_new.className = "visible for-author-only in-panel";
            brickpanel_edit.className = "invisible for-author-only in-panel";
            $(".in-panel").addClass("editing");
            $(".in-panel input").prop("readonly", false);
            $("#selectPrivacy").attr("disabled", false);
            $("#inputDate").attr("disabled", false);
            $("#checkboxHasNumber").attr("disabled", false);
            break;

        case MarkerStates.COMMON:
            author_name.className = "visible in-panel";
            photo_uploader.className = "invisible in-panel";
            brickpanel_common.className = "visible for-author-only in-panel";
            brickpanel_new.className = "invisible for-author-only in-panel";
            brickpanel_edit.className = "invisible for-author-only in-panel";
            $(".in-panel").removeClass("editing");
            $(".in-panel input").prop("readonly", true);
            $(".in-panel textarea").prop("readonly", true);
            $("#selectPrivacy").attr("disabled", true);
            $("#inputDate").attr("disabled", true);
            $("#checkboxHasNumber").attr("disabled", true);
            break;

        case MarkerStates.EDIT:
            author_name.className = "invisible in-panel";
            photo_uploader.className = "visible in-panel";
            brickpanel_common.className = "invisible for-author-only in-panel";
            brickpanel_new.className = "invisible for-author-only in-panel";
            brickpanel_edit.className = "visible for-author-only in-panel";
            $(".in-panel").addClass("editing");
            $(".in-panel input").prop("readonly", false);
            $(".in-panel textarea").prop("readonly", false);
            $("#selectPrivacy").attr("disabled", false);
            $("#inputDate").attr("disabled", false);
            $("#checkboxHasNumber").attr("disabled", false);
            break;
    }
}

BrickPanel.prototype.Open = function () {
    if (this.isOpen)
        return;
    this.isOpen = !this.isOpen;
    this.panel.className = "shown";
}

BrickPanel.prototype.Close = function () {
    if (!this.isOpen)
        return;
    this.isOpen = !this.isOpen;
    this.panel.className = "hidden";
}
