﻿var MenuSwitcher = {
    isOpen: true,
    Switch: function () {
        var menu = document.getElementById("menu");
        var switcher = document.getElementById("menu_switcher");
        this.isOpen = !this.isOpen;

        if (this.isOpen) {
            menu.className = "shown";
            switcher.className = "";
            switcher.src = "../Images/up.png";
        }
        else {
            menu.className = "hidden";
            switcher.className = "hidden";
            switcher.src = "../Images/down.png";
        }
    }
};

