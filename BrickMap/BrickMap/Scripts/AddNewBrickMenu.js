﻿// Script for 'add new brick' circle menu 

console.log("AddNewBrickMenu.js loaded");

(function () {
    var button = document.getElementById('addnew-button');
    var wrapper = document.getElementById('addnew-wrapper');

    //open and close menu when the button is clicked
    var open = false;
    button.addEventListener('click', handler, false);
    wrapper.addEventListener('click', cnhandle, false);

    var centre_btn = document.getElementById('in-centre-mode');
    var location_btn = document.getElementById('by-location-mode');
    var coords_btn = document.getElementById('by-coords-mode');

    centre_btn.addEventListener("click", SetAddInMapCentreMode);
    location_btn.addEventListener("click", SetAddByLocationMode);
    coords_btn.addEventListener("click", SetAddByCoordsMode);

    function cnhandle(e) {
        e.stopPropagation();
    }

    function handler(e) {
        if (!e) var e = window.event;
        e.stopPropagation();//so that it doesn't trigger click event on document

        if (!open) {
            openNav();
        }
        else {
            closeNav();
        }
    }
    function openNav() {
        open = true;
        $("#addnew-wrapper li").addClass("shown");
    }
    function closeNav() {
        open = false;
        $("#addnew-wrapper li").removeClass("shown");
    }
    
})();

