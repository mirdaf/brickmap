﻿console.log("loaded BrickMapMenu.js");

window.onload = InitMenu;

function InitMenu() {
    var login = document.getElementById("login");
    login.addEventListener("click", OpenLoginForm);
    var go_back = document.getElementById("go_back");
    go_back.addEventListener("click", RemoveLoginForm);
    var sandwich = document.getElementById("menu-sandwich");
    sandwich.addEventListener("click", function () { SandwichMenuSwitcher.Switch(); });
}

function OpenLoginForm() {
    document.getElementById("unsigned_box").className = "invisible";
    document.getElementById("login_form").className = "visible";
    document.getElementById("logged_in_form").className = "invisible";
    SandwichMenuSwitcher.ReorderItems();
}

function RemoveLoginForm() {
    document.getElementById("unsigned_box").className = "visible";
    document.getElementById("login_form").className = "invisible";
    document.getElementById("logged_in_form").className = "invisible";
}

var SandwichMenuSwitcher = {
    isShown: false,
    Switch: function () {
        this.isShown = !this.isShown;

        if (this.isShown) {
            $(".menu-right").addClass("menu-right-shown");
            $(".top-validator").removeClass("top-validator-hidden");
            this.ReorderItems();
        }
        else {
            $(".menu-right").removeClass("menu-right-shown");
            $(".top-validator").addClass("top-validator-hidden");
        }
    },
    ReorderItems: function () {
        var items = $(".visible .menu-right");
        var number = items.length;
        for (var i = 0; i < number; i++) {
            var order = number - i;
            items[i].style["-webkit-box-ordinal-group"] = order; /* старый синтаксис — iOS 6-, Safari 3.1-6 */
            items[i].style["-moz-box-ordinal-group"] = order; /* старый синтаксис — Firefox 19- */
            items[i].style["-ms-flex-order"] = order; /* промежуточный синтаксис 2011 года — IE 10 */
            items[i].style["-webkit-order"] = order; /* новый синтаксис — Chrome */
            items[i].style["order"] = order;
        }
    }
};
