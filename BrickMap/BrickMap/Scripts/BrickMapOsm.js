﻿var map;
var cluster; // for clustering markers
var marker; // only for adding new bricks
var marks = {};
var dragging_mark;
var AddNewBrickButton;
var defaultLatLng = [59.93917, 30.31583]; // Palace Square,  SPb.

var common_icon = null;
var selected_icon = null;
var newbrick_icon = null;

function loadScript(url, callback) {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                if (callback)
                    callback();
            }
        };
    } else {  //Others
        script.onload = function () {
            if (callback)
                callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

loadScript("http://cdn.leafletjs.com/leaflet/v0.7.7/leaflet.js", function () {
    var map_container = document.getElementById("map");
    if (map_container === null) {
        setTimeout(function () {
            InitMap();
        }, 1000);
    }
    else
        InitMap();
});

function InitMap() {
    map = L.map('map'); // maxZoom = 18

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '<a href="http://osm.org/copyright">OpenStreetMap</a>'
    }).addTo(map);
    
    loadScript("../vendors/MarkerCluster/leaflet.markercluster-src.js",
        function () {
            loadScript("../vendors/MarkerCluster/leaflet.markercluster.js", function () {
                loadScript("../Scripts/BrickMark.js",
                function () {
                    BrickMap.MyDbService.GetBricks(ShowBrickMarks, OnError);
                });
        });
    });
    
    InitBrickPanel();
    BrickPanelControls.Init();

    common_icon = L.icon({
        iconUrl: '../Images/marker-red.png',
        //iconRetinaUrl: 'my-icon@2x.png',
        iconSize: [25, 41],
        iconAnchor: [12, 39],
        popupAnchor: [0, -28],
        shadowUrl: 'http://cdn.leafletjs.com/leaflet/v0.7.7/images/marker-shadow.png ',
        //shadowRetinaUrl: 'my-icon-shadow@2x.png',
        //shadowSize: [68, 95],
        //shadowAnchor: [22, 94]
    });

    selected_icon = L.icon({
        iconUrl: '../Images/marker-blue.png',
        //iconRetinaUrl: 'my-icon@2x.png',
        iconSize: [25, 41],
        iconAnchor: [12, 39],
        popupAnchor: [0, -28],
        shadowUrl: 'http://cdn.leafletjs.com/leaflet/v0.7.7/images/marker-shadow.png',
        //shadowRetinaUrl: 'my-icon-shadow@2x.png',
        //shadowSize: [68, 95],
        //shadowAnchor: [22, 94]
    });

    newbrick_icon = L.icon({
        iconUrl: '../Images/marker-white.png',
        //iconRetinaUrl: 'my-icon@2x.png',
        iconSize: [25, 41],
        iconAnchor: [12, 39],
        popupAnchor: [0, -28],
        shadowUrl: 'http://cdn.leafletjs.com/leaflet/v0.7.7/images/marker-shadow.png',
        //shadowRetinaUrl: 'my-icon-shadow@2x.png',
        //shadowSize: [68, 95],
        //shadowAnchor: [22, 94]
    });

    document.body.onkeydown = function (e)
    {
        if (e.keyCode == 27) { // esc
            $('#inputDate').pickmeup('hide');
            map.closePopup();
            HideBrickPanel();
        }
        e.returnValue = true;
    };
}

function ShowBrickMarks(result) {

    cluster = L.markerClusterGroup({
        spiderfyOnMaxZoom: true,
        iconCreateFunction: function (cluster) {
            var markers = cluster.getAllChildMarkers();
            var n = markers.length;
            return L.divIcon({ html: n, className: 'brick-cluster', iconSize: L.point(36, 36) });
        },
    });

    var mark_id_el = L.DomUtil.get("mark_id");
    var request_mark_id = mark_id_el.value;
    var request_mark_available = false;

    var LatLngArray = [];

    for (var n = 0; n < result.length; n++) {
        var mark_id = result[n].Id;
        if (request_mark_id == mark_id)
            request_mark_available = true;

        var lat = result[n].Coordinates.Latitude;
        var lon = result[n].Coordinates.Longitude;
        LatLngArray.push([lat, lon]);

        marks[mark_id] = brickMark([lat, lon], {
            icon: common_icon,
            riseOnHover: true
        }, mark_id, true);

        cluster.addLayer(marks[mark_id]);
    }

    map.addLayer(cluster);

    if (LatLngArray.length > 0)
        map.fitBounds(LatLngArray);
    else
        map.setView(defaultLatLng, 13);

    if (request_mark_available) {
        marks[request_mark_id].TryShowCommonBrickPanel();
        cluster.zoomToShowLayer(marks[request_mark_id]);
    }
}

function RemoveMarkFromMap(mark_id) {
    HidePreloader();
    console.log("mark deleted");
    cluster.removeLayer(marks[mark_id]);
    //marks[mark_id].onRemove(map);
    HideBrickPanel();
}

function OnError(result) {
    HidePreloader();
    console.log(result);
}

function CreateAddNewBrickButton() {
    AddNewBrickButton = L.Control.extend({
        options: {
            position: 'topleft'
        },

        onAdd: function (map) {
            var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
            container.id = "add_new_brick_div";
            var button = L.DomUtil.create('a', '');
            button.id = "add_new_brick_button";
            button.href = "#";
            button.title = "Add new brick on map";
            button.style.width = "auto";
            button.innerHTML = "<span>new</span>";
            button.addEventListener("click", function () { AddNewBrickController.Switch(); });
            container.appendChild(button);
            container.addEventListener("click", function (e) { e.stopPropagation(); });
            return container;
        }
    });
}
var AddNewBrickController = {
    ifAddButtonsShown: false,
    
    Switch: function () {
        this.ifAddButtonsShown = !this.ifAddButtonsShown;
        if (this.ifAddButtonsShown)
            this.ShowAddButtons();
        else
            this.HideAddButtons();
    },

    ShowAddButtons: function () {
        var container = L.DomUtil.get("add_new_brick_div");
        var button = L.DomUtil.get("add_new_brick_button");
        button.innerHTML = "<span>cancel</span>";
        button.title = "Cancel";

        var setByClick = L.DomUtil.create('a', '', container);
        setByClick.id = "set_by_click";
        setByClick.href = "#";
        setByClick.title = "Set the mark by pointer";
        setByClick.style.width = "auto";
        //setByClick.style.min-width = 30; //TODO in css
        setByClick.innerHTML = "<span>by pointer</span>";
        setByClick.addEventListener("click", SetAddInMapCentreMode);

        var setByMyLocation = L.DomUtil.create('a', '', container);
        setByMyLocation.id = "set_by_my_location";
        setByMyLocation.href = "#";
        setByMyLocation.title = "Find my location and set the mark";
        setByMyLocation.style.width = "auto";
        setByMyLocation.innerHTML = "<span>by location</span>";
        setByMyLocation.addEventListener("click", SetAddByLocationMode);

        var setByDirectCoords = L.DomUtil.create('a', '', container);
        setByDirectCoords.id = "set_by_coords";
        setByDirectCoords.href = "#";
        setByDirectCoords.title = "Set the mark by latitude and longitude";
        setByDirectCoords.style.width = "auto";
        setByDirectCoords.innerHTML = "<span>by coordinates</span>";
        setByDirectCoords.addEventListener("click", SetAddByCoordsMode);
    },

    HideAddButtons: function () {
        HideAddingNewBrickDialog();
        var button = L.DomUtil.get("add_new_brick_button");
        button.innerHTML = "<span>new</span>";
        button.title = "Add new brick";

        var ob = L.DomUtil.get("set_by_click");
        ob.parentNode.removeChild(ob);
        ob = L.DomUtil.get("set_by_my_location");
        ob.parentNode.removeChild(ob);
        ob = L.DomUtil.get("set_by_coords");
        ob.parentNode.removeChild(ob);

        var m = document.getElementById("map");
    }
};

function SetAddInMapCentreMode() {
    if (UserIsLoggedIn()) {
        map.closePopup();
        ShowAddNewBrickDialog(map.getCenter());
    }
    else {
        ShowNotification("Авторизуйтесь, чтобы добавлять метки");
    }
}

function UserIsLoggedIn() {
    var user_auth = L.DomUtil.get("UserAuth");
    if (user_auth.value == "0")
        return false;
    else 
        return true;
}

function RemoveAddInMapCentreMode() { // 
    map.closePopup();
    if (marker) {
        marker.onRemove(map);
        NullifyMarker();
        HideBrickPanel();
    }
}

function SetAddByLocationMode() {
    if (!UserIsLoggedIn()) {
        ShowNotification("Авторизуйтесь, чтобы добавлять метки");
        return;
    }
    RemoveAddInMapCentreMode();
    map.locate({
        setView: true,
        enableHighAccuracy: true,
    });
    map.addEventListener("locationfound", AddBrickOnFoundLocation);
    map.addEventListener("locationerror", OnLocationError);
}

function SetAddByCoordsMode() {
    if (!UserIsLoggedIn()) {
        ShowNotification("Авторизуйтесь, чтобы добавлять метки");
        return;
    }
    RemoveAddInMapCentreMode();
    var p_content = L.DomUtil.create('div', 'dialog-popup');
    var input = L.DomUtil.create('h3', '', p_content);
    input.innerHTML = "Введите координаты метки";

    var p = L.DomUtil.create('p', '', p_content);
    L.DomUtil.create('label', '', p)
        .innerHTML = "Широта:";
    input = L.DomUtil.create('input', '', p);
    input.type = "text";
    input.placeholder = "59.93917";
    input.id = "txtLat";
    
    p = L.DomUtil.create('p', '', p_content);
    L.DomUtil.create('label', '', p)
        .innerHTML = "Долгота:";
    input = L.DomUtil.create('input', '', p);
    input.type = "text";
    input.placeholder = "30.31583";
    input.id = "txtLon";

    var btnOK = L.DomUtil.create('a', 'panel-btn', p_content);
    btnOK.id = "btnOK";
    btnOK.href = "#";
    btnOK.title = "Set mark on coordinates";
    btnOK.style.width = "auto";
    btnOK.innerHTML = "<span>OK</span>";
    btnOK.addEventListener("click", ShowAddingPopupByCoords);

    var btnClose = L.DomUtil.create('a', 'panel-btn', p_content);
    btnClose.id = "btnClose";
    btnClose.href = "#";
    btnClose.title = "Закрыть";
    btnClose.style.width = "auto";
    btnClose.innerHTML = "<span>Отмена</span>";
    btnClose.addEventListener("click", function () { map.closePopup(); });

    var popupCoords = L.popup().setLatLng(map.getCenter())
        .setContent(p_content)
        .openOn(map);
}

function ShowAddingPopupByCoords() {
    var lat = L.DomUtil.get("txtLat").value;
    var lon = L.DomUtil.get("txtLon").value;
    if (CoordsAreValid(lat, lon))
        ShowAddNewBrickDialog({ "lat": lat, "lng": lon });
}

function CoordsAreValid(lat, lon) {
    if (!isNaN(parseFloat(lat)) && isFinite(lat)) {
        if (lat < -90 || lat > 90) {
            ShowNotification("Широта - число от -90 до 90 градусов", "#txtLat");
            return false;
        }
    }
    else {
        ShowNotification("Широта - число от -90 до 90 градусов", "#txtLat");
        return false;
    }

    if (!isNaN(parseFloat(lon)) && isFinite(lon)) {
        if (lon < -180 || lon > 180) {
            ShowNotification("Долгота - число от -180 до 180 градусов", "#txtLon");
            return false;
        }
    }
    else {
        ShowNotification("Долгота - число от -180 до 180 градусов", "#txtLon");
        return false;
    }
    return true;
}

function AddBrickOnFoundLocation() {
    map.removeEventListener("locationfound", AddBrickOnFoundLocation);
    map.removeEventListener("locationerror", OnLocationError);
    var coords = map.getCenter();
    ShowAddNewBrickDialog(coords);
}

function OnLocationError() {
    ShowNotification("location not found, please try later");
}

function ShowAddNewBrickDialog(coords) {
    console.log("ShowAddNewBrickDialog");
    if (marker)
        return;
    marker = brickMark([coords.lat, coords.lng], {
        icon: newbrick_icon,
        riseOnHover: true,
        draggable: true,
        riseOffset: 200
    }, null, false);
    console.log(marker);
    marker.SetBrick(new Brick());
    marker.ShowNewBrickPanel();
}

function HideAddingNewBrickDialog() {
    HideBrickPanel();
    if (marker) {
        marker.onRemove(map);
        NullifyMarker();
    }
}

var NewBrick = {
    Id: 0,
    Stamp: null,
    Number: 0,
    HasNumber: false,
    Quality: null,
    DiscoveryDate: null,
    //Photos
    //Author
    Mark: 0, //?
    Privacy: 0,
    Comment: null,
    Lat: 0,
    Lon: 0
};

function NullifyMarker() {
    marker = null;
}
