﻿var MarkerStates = Object.freeze({
    NEW: 0,
    COMMON: 1,
    EDIT: 2,
});
var gBrickPanel = null;

//$(function () {
//    var txt = $('#txtComment');
//    console.log(txt);
//    var hiddenDiv = $(document.createElement('div'));
//    var content = null;
//    txt.addClass('noscroll');
//    hiddenDiv.addClass('hidden-comment');
	 
//    $('body').append(hiddenDiv);
//    txt.bind('keyup', function () {
//        console.log('keyup');
//        content = txt.val();
//        content = content.replace(/\n/g, '<br>');
//        hiddenDiv.html(content);
//        txt.css('height', hiddenDiv.height());
//    });
//});

function InitBrickPanel() { 
    gBrickPanel = new BrickPanel();
    gBrickPanelContainer = document.getElementById("brickpanel");

    L.DomEvent.disableClickPropagation(gBrickPanelContainer);
    L.DomEvent.disableScrollPropagation(gBrickPanelContainer);

    var close_button = document.getElementById("close_panel");
    close_button.addEventListener('click', HideBrickPanel);

    $("#checkboxHasNumber").change(function () {
        if (this.checked){
            SetCheckedMode();
        }
        else {
            SetUncheckedMode();
        }
    })

    var stars = document.getElementsByClassName("star");
    var len = stars.length;
    for (var i = 0; i < len; ++i) {
        stars[i].setAttribute("q-rating", i + 1);
    }

    EnableDatePicker();
}

function SetCheckedMode() {
    $(".depend-on-checkbox-has-number").removeClass('hidden');
    $("#lblForHasNumber").addClass('checked');
}
function SetUncheckedMode() {
    $(".depend-on-checkbox-has-number").addClass('hidden');
    $("#lblForHasNumber").removeClass('checked');
}

var EnableDatePicker = function () {
    $('#inputDate').pickmeup({
        hide_on_select: true,
        position: 'top',
        format: 'd-m-Y',
    });
    $('#inputDate').on('blur', function () {
        $(this).pickmeup('hide');
    });
}

function ShowBrickPanel() {
    gBrickPanel.Open();
}

function HideBrickPanel(remove_selection) {
    gBrickPanel.Close();
    if (remove_selection !== false && BrickMark.selected_mark_id) {
        setTimeout(function () {
            marks[BrickMark.selected_mark_id].RemoveSelectedMarkState();
        }, 1300);
    }
}
