﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrickMap
{
    public partial class RecoverPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (BrickCookiesHelper.IsUserLoggedIn())
                Response.Redirect("~/Default.aspx");
            confirmRecoverPassword.Click += (o, ev) => SendEmailForRecovery();
        }

        private void SendEmailForRecovery()
        {
            string email = txtEmail.Text;
            if (email == "")
                return;
            LoginService loginService = new LoginService();
            int user_id = loginService.CheckIfEmailIsRegistered(txtEmail.Text);

            if (user_id == -1)
            {
                string script = "<script type='text/jscript'>ShowNotification('Пользователь с таким e-mail не зарегистрирован');</script>";
                Type type = GetType();
                if (!Page.ClientScript.IsStartupScriptRegistered(type, "NotRegEmailScript"))
                    Page.ClientScript.RegisterStartupScript(type, "NotRegEmailScript", script);
            }
            else
            {
                string guid = loginService.CreateGuidForRecovery(user_id);
                string link = "brickmap.ru/ResetPassword.aspx?guid=" + guid;
                BrickMailService.SendMailToRecoverPassword(email, link);
                txtEmail.Text = "";
                ShowMessage("Информация выслана на указанный email");
            }
        }
        private void ShowMessage(string msg)
        {
            txtMessage.Text = msg;
        }
    }
}