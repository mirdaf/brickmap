﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace BrickMap
{
    public class BrickDebugger
    {
        static private FileInfo file;
        static BrickDebugger dbg = null;
        static int numero = 0;
        public static BrickDebugger GetDebugger(string file_path)
        {
            if(dbg == null)
                dbg = new BrickDebugger(file_path);
            return dbg;
        }

        private BrickDebugger(string file_path)
        {
            file = new FileInfo(file_path);
            file.Create().Close();
        }
        
        public void WriteToLog(string value)
        {
            using (var writer = file.AppendText())
            {
                writer.WriteLine(value);
                writer.Close();
            }
        }
    }
}