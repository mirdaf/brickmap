﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace BrickMap
{
    public class BrickCookiesHelper
    {
        public static void RegisterBrickUserCookies(DB.User user, bool isPersistent)
        {
            FormsAuthentication.SetAuthCookie(user.Email, isPersistent);
            HttpCookie cookie = FormsAuthentication.GetAuthCookie(user.Email, isPersistent);
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
            FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(
                 ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration
                , ticket.IsPersistent, user.ToJson(), ticket.CookiePath
            );
            string encTicket = FormsAuthentication.Encrypt(newTicket);
            cookie.Value = encTicket;
            System.Web.HttpContext.Current.Response.Cookies.Add(cookie);
        }
        public static DB.User GetUserData()
        {
            string userData = ((FormsIdentity)System.Web.HttpContext.Current.User.Identity).Ticket.UserData;
            return BrickMap.DB.User.getFromJson(userData);
        }
        public static void SaveNewUserLogin(string login)
        {
            DB.User user = BrickCookiesHelper.GetUserData();
            if(user.Login != login)
            {
                user.Login = login;
                BrickCookiesHelper.RegisterBrickUserCookies(user, false);
            }
        }
        public static bool IsUserLoggedIn()
        {
            return System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
        }
    }
}