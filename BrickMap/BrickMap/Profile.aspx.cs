﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BrickMap
{
    public partial class Profile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!BrickCookiesHelper.IsUserLoggedIn())
                Response.Redirect("~/Register.aspx");

            if (!IsPostBack)
            {
                DB.User user = BrickCookiesHelper.GetUserData();
                txtLogin.Text = user.Login;
                txtEmail.Text = user.Email;
                txtEmail.ReadOnly = true;
                txtErrorMessage.Text = "";
            }
            submitEditProfile.Click += (o, ev) => TrySaveProfile();
        }
        private void TrySaveProfile()
        {
            txtErrorMessage.Text = "";
            bool result;
            string login = txtLogin.Text;
            if (String.IsNullOrEmpty(txtPassword.Text) && String.IsNullOrEmpty(txtPasswordDouble.Text))
            {
                result = (new LoginService()).SaveNewLogin(txtEmail.Text, txtLogin.Text);
            }
            else
            {
                if(String.Compare(txtPassword.Text, txtPasswordDouble.Text) != 0)
                {
                    ComparePasswordsValidator.IsValid = false;
                    return;
                }
                string hash = FormsAuthentication.HashPasswordForStoringInConfigFile(txtPassword.Text, "SHA1");
                result = (new LoginService()).SaveEditedUser(txtEmail.Text, hash, login);
            }
            if (result)
                BrickCookiesHelper.SaveNewUserLogin(txtLogin.Text);
            else
                ShowErrorMessage("Не удалось сохранить изменения, повторите попытку позже.");
        }

        private void ShowErrorMessage(string msg)
        {
            txtErrorMessage.Text = msg;
        }
    }
}
